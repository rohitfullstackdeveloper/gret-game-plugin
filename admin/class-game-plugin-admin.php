<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/

class WPLI_Admin {
    private $plugin_name,$version,$helper,$comment_list;
    public $options_group = 'reading';
    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
        $this->helper = new game_plugin_helper();        
        add_action('admin_menu', array($this, 'game_admin_menu' ));        
        add_action('admin_enqueue_scripts', array($this,'cstm_css_and_js_tte'));
        add_action( 'admin_init', array($this,'dbi_register_settings' ));
        add_action( 'admin_enqueue_scripts', array($this,'mw_enqueue_color_picker' ));
        add_action( 'wp_ajax_wpdocs_action', array($this,'wpdocs_action_function' ));
        //add_action( 'cmb2_admin_init', array($this,'wpli_register_main_options_metabox' ));
        add_action( 'wp_ajax_nopriv_playgret_spin_wheel_settingform',  array( $this,'playgret_spin_wheel_settingformhandler' ));
        add_action( 'wp_ajax_playgret_spin_wheel_settingform',  array( $this,'playgret_spin_wheel_settingformhandler' ));
        add_action( 'wp_ajax_nopriv_playgretspinwheelsegmentsform',  array( $this,'playgretspinwheelsegmentsformhandler' ));
        add_action( 'wp_ajax_playgretspinwheelsegmentsform',  array( $this,'playgretspinwheelsegmentsformhandler' ));
        add_action( 'wp_ajax_segmentlistingedit',  array($this,'segmentlistingedithandler') );
        add_action( 'wp_ajax_nopriv_segmentlistingedit',  array($this,'segmentlistingedithandler') );
        add_action( 'wp_ajax_playgretspinwheelsegmentUpadateform',  array($this,'playgretspinwheelsegmentUpadateformhandler') );
        add_action( 'wp_ajax_nopriv_playgretspinwheelsegmentUpadateform',  array($this,'playgretspinwheelsegmentUpadateformhandler') );
        add_action( 'wp_ajax_nopriv_segmentlistingedit',  array($this,'segmentlistingedithandler') );
        add_action( 'wp_ajax_playgretspinwheelsegmentDeleteform',  array($this,'playgretspinwheelsegmentDeleteformhandler') );
        add_action( 'wp_ajax_nopriv_playgretspinwheelsegmentDeleteform',  array($this,'playgretspinwheelsegmentDeleteformhandler') );
    }

    public function playgretspinwheelsegmentDeleteformhandler(){
        global $wpdb; 
        $table_name_segment = $wpdb->prefix . "spinwheelsegments";
        $id = $_REQUEST['delete_val'];
        $result = $wpdb->delete( $table_name_segment, array( 'id' => $id ));
        $response['success'] = 1;
        $response['message'] = 'Deleted Succesfully';
        echo json_encode($response); exit;
    }
    public function playgretspinwheelsegmentUpadateformhandler(){
        global $wpdb; 
        $table_name_segment = $wpdb->prefix . "spinwheelsegments";
        $request_array  =  array();
        $request_array[ 'txtdisplaytext' ]      = $_REQUEST['txtdisplaytextUpdate'];
        $request_array[ 'segmenttext' ]         = $_REQUEST['segmenttextUpdate'];
        $request_array[ 'txtbackgroundcolor' ]  = $_REQUEST['txtbackgroundcolorUpdate'];
        $request_array[ 'ddlgravity' ]          = $_REQUEST['ddlgravityUpdate'];
        $request_array[ 'hdngravityperc' ]      = $_REQUEST['hdngravitypercUpdate'];
        $request_array[ 'iscouponcode' ]        = $_REQUEST['iscouponcodeUdpate'];
        $request_array[ 'couponcode' ]          = $_REQUEST['couponcodeUpate'];
        
        // echo'<pre>';print_r($request_array);exit;
        $wpdb->update($table_name_segment, $request_array, array('id' => $_REQUEST['update_save_val']));

        $response['success'] = 1;
        $response['message'] = 'Updated Succesfully';
        // $sql = $wpdb->query($wpdb->prepare("UPDATE `$table` SET comment ='$updateComments' WHERE id ='$addId'"));
        echo json_encode($response); exit;
        
    }
    public function segmentlistingedithandler(){
        global $wpdb; 
        $table_name_segment = $wpdb->prefix . "spinwheelsegments";
        $id = $_REQUEST['id'];        
        $result = $wpdb->get_row($wpdb->prepare("SELECT * from $table_name_segment WHERE id=".$id." LIMIT 1" ));
        // echo'<pre>';print_r($_REQUEST);exit;
        if(!empty($result)){
            $response['id'] = $result->id;
            $response['txtdisplaytext'] = $result->txtdisplaytext;
            $response['segmenttext'] = $result->segmenttext;
            $response['txtbackgroundcolor'] = $result->txtbackgroundcolor;
            $response['ddlgravity'] = $result->ddlgravity;
            $response['hdngravityperc'] = $result->hdngravityperc;
            $response['iscouponcode'] = $result->iscouponcode;
            $response['couponcode'] = $result->couponcode;
            $response['success'] = 1;
        }else{
            $response['success'] = 0;
        }
        echo json_encode($response); exit;
    }
    public function wpdocs_action_function(){
        check_ajax_referer( 'wgp_ajax_nonce', 'lpNonce' );
        // Nonce is checked, get the POST data and sign user on
        if( !wp_verify_nonce(sanitize_text_field($_POST['lpNonce']), 'wgp_ajax_nonce')) {
            $res = json_encode(array('nonceerror'=>'yes'));
            die($res);
        }
    }
    public function playgretspinwheelsegmentsformhandler(){
        //echo "<pre>";print_r($_REQUEST);exit;
        global $wpdb;
        $table_name_segment = $wpdb->prefix . "spinwheelsegments";       
        $wgp_segmentform_options = array(
            'txtdisplaytext'  => sanitize_text_field($_POST['txtdisplaytext']),
            'segmenttext'   => sanitize_text_field($_POST['segmenttext']),
            'txtbackgroundcolor'   => sanitize_text_field($_POST['txtbackgroundcolor']),
            'ddlgravity'   => sanitize_text_field($_POST['ddlgravity']),
            'hdngravityperc'   => sanitize_text_field($_POST['hdngravityperc']),
            'iscouponcode'   => sanitize_text_field($_POST['iscouponcode']),
            'couponcode'   => sanitize_text_field($_POST['couponcode'])
        );   
        $result = $wpdb->get_row($wpdb->prepare("SELECT * from $table_name_segment")); 
        //echo "<pre>";print_r($result);exit;
        $wpdb->insert($table_name_segment,$wgp_segmentform_options);
        // if($result->id !=''){
        // $wpdb->update($table_name_segment,$wgp_segmentform_options,array('id'=>$result->id));
        // }
        $response[ 'success' ]  = "1";
        $response[ 'msg' ]  = 'Data Inserted Successfully!';
        echo json_encode( $response );exit;
    }
    public function playgret_spin_wheel_settingformhandler(){
        global $wpdb;

        
        $app_filename = '';$app_filename_icon = '';
        $allowed_ext = array("jpg", "jpeg", "png", "gif");
        if($_FILES['files']['name'] == '' && $_FILES['files']['name'] != $allowed_ext){		
            $error['files'] = 'Please Upload Images and only allowed "jpg", "jpeg", "png", "gif"';
        }elseif($_FILES['markericon']['name'] == '' && $_FILES['markericon']['name'] != $allowed_ext){		
            $error['markerfiles'] = 'Please Upload Images and only allowed "jpg", "jpeg", "png", "gif"';
        }

        if(is_array($_FILES) && !empty($_FILES)){
            $file_name = explode(".", $_FILES['files']['name']);
            
            if(in_array($file_name[1], $allowed_ext)){
                $filename = $_FILES['files']['name'];                
                $targetPath =   WGP_APP_PATH.'uploads/gallery/'.$filename;                
                $sourcePath = $_FILES['files']['tmp_name'];  
                $app_filename = $_FILES['files']['name'];         
                $app_url = WGP_APP_URL.'uploads/gallery/'.$filename;
                move_uploaded_file($sourcePath, $targetPath);
                //echo'<pre>';print_r($app_filename);exit;
            }else{
                $response   =   array(
                    'message' => 'Error',
                );
            }
            $file_name = explode(".", $_FILES['markericon']['name']);
            if(in_array($file_name[1], $allowed_ext)){
                $filename = $_FILES['markericon']['name'];
                $targetPath =   WGP_APP_PATH.'uploads/icon/'.$filename;
                $sourcePath = $_FILES['markericon']['tmp_name'];
                $app_filename_icon = $_FILES['markericon']['name'];
                $app_url = WGP_APP_URL.'uploads/icon/'.$filename;
                move_uploaded_file($sourcePath, $targetPath);
                // echo'<pre>';print_r($app_filename_icon);exit;    
            }else{
                $response   =   array(
                    'message' => 'Error',
                );
            }
        }
        $table_name = $wpdb->prefix . "spinwheelsettings";
        //echo "<pre>";print_r($_REQUEST);exit;
        if(!$error){
            $wgp_settingsform_options = array(
                'outerradius' 					    => sanitize_text_field($_POST['outerradius']),
                'innerradius' 					    => sanitize_text_field($_POST['innerradius']),
                'textfontsize' 					    => sanitize_text_field($_POST['textfontsize']),
                'textmargin' 					    => sanitize_text_field($_POST['textmargin']),
                'whitetextcolor' 				    => sanitize_text_field($_POST['whitetextcolor']),
                'whitestrokecolor' 				    => sanitize_text_field($_POST['whitestrokecolor']),
                'wheelstrokewidth'				    => sanitize_text_field($_POST['wheelstrokewidth']),
                'gameovertext'				        => sanitize_text_field($_POST['gameovertext']),
                'introtext'				            => sanitize_text_field($_POST['introtext']),
                'countdowntime'				        => sanitize_text_field($_POST['countdowntime']),
                'enablediscountbar'				    => sanitize_text_field($_POST['enablediscountbar']),
                'desktopintervaltext'				=> sanitize_text_field($_POST['desktopintervaltext']),
                'mobileintervaltext'				=> sanitize_text_field($_POST['mobileintervaltext']),
                'cookiedays'				        => sanitize_text_field($_POST['cookiedays']),
                'showdesktop'				        => sanitize_text_field($_POST['showdesktop']),
                'desktopintent'				        => sanitize_text_field($_POST['desktopintent']),
                'desktopinterval'				    => sanitize_text_field($_POST['desktopinterval']),
                'showmobile'				        => sanitize_text_field($_POST['showmobile']),
                'mobileintent'				        => sanitize_text_field($_POST['mobileintent']),
                'position'				            => sanitize_text_field($_POST['position']),
                'bg_image' 						    => $app_filename,
                'marker_image'                      => $app_filename_icon
            );
            //echo "<pre>";print_r($wgp_settingsform_options);exit;
            $result = $wpdb->get_row($wpdb->prepare("SELECT * from $table_name"));        
            //echo "<pre>";print_r($result);
            if($result->id != ''){            
                $wpdb->update($table_name,$wgp_settingsform_options,array('id'=>$result->id));            
              //echo "<pre>";echo "update";exit;
            }else{                        
                $wpdb->insert($table_name,$wgp_settingsform_options);    
                //echo "<pre>";echo "insert";exit;
            }
            $response[ 'success' ]  = "1";
            $response[ 'msg' ]  = 'Data Inserted Successfully!';
        }
        echo json_encode( array('data'=>$response,'error'=>$error ));exit;
    }
    function mw_enqueue_color_picker( $hook_suffix ) {
         // first check that $hook_suffix is appropriate for your admin page
         wp_enqueue_style( 'wp-color-picker' );
         wp_enqueue_script( 'color-picker-script-handle', WGP_APP_URL.'public/assets/js/color-picker-script.js', array( 'wp-color-picker' ), false, true );
    }
    public function cstm_css_and_js_tte(){
        wp_enqueue_style('wgp-admin-css-frontend-game', WGP_APP_URL.'public/assets/admin/css/admin-css.css', array(), false, false);
        wp_enqueue_script( 'wgp_sweet_alert_js','https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.js');
        wp_enqueue_style ( 'wgp_sweet_alert_style', 'https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/11.0.19/sweetalert2.min.css');
        wp_enqueue_script( 'wgp_settings_custom_js', plugins_url( 'public/assets/admin/js/playgret-admin.js', WGP_APP_RELPATH ), array( 'jquery' ), '1.3' );
        wp_enqueue_style('wgp-frontend-font-awesome4.0', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
        wp_enqueue_style('wgp-frontendselect2.0', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');
        wp_enqueue_script( 'wgp-frontendselect-js-2.0', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
        wp_localize_script( 'wgp_settings_custom_js', 'settings_all_ajax_object', array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }
    public function game_admin_menu(){
        add_menu_page(__('Gret Game Settings','WPLI'),__('Gret Game Settings','WPLI'),'manage_options','game-slug',array($this,'wpli_menu_games'));  
    }
    public function wpli_menu_games() {
        if ( ! current_user_can( 'manage_options' ) ) {
            return;
        }
        if ( isset( $_GET['settings-updated'] ) ) {
            add_settings_error( 'game_messages', 'game_message', __( 'Settings Saved', 'game' ), 'updated' );
        }
        settings_errors( 'game_messages' );
        ?>
        <div class="wrap">            
            <form action="options.php" method="post" id="game-form">
                <?php
                settings_fields( 'game' );
                do_settings_sections( 'game' );
                submit_button( 'Save Settings' );
                ?>    
            </form>
        </div>
    <?php }   
    public function dbi_register_settings() {   
        register_setting( 'game', 'game_options' );
        add_settings_section('game_section_developers',
        __( 'Settings', 'game' ), array($this,'game_section_developers_callback'),
            'game'
        );
        add_settings_field(
            'game_start_popup_seconds',
            __( 'Start popup seconds', 'game' ),
            array($this,'game_field_start_popup_seconds'),
            'game',
            'game_section_developers',
            array(
                'label_for'         => 'game_start_popup_seconds',
                'class'             => 'game_row'
            )
        );
        add_settings_section('game_section_google_developers',
        __( 'Google oauth settings', 'game' ), array($this,'game_section_google_developers_callback'),
            'game'
        );
        add_settings_field(
            'game_google_api_keys',
            __( 'Api Key:', 'game' ),
            array($this,'game_field_google_api_settings'),
            'game',
            'game_section_google_developers',
            array(
                'label_for'         => 'game_google_api_keys',
                'class'             => 'game_row'
            )
        );
        add_settings_field(
            'game_google_secret_keys',
            __( 'Secret Key:', 'game' ),
            array($this,'game_field_google_secret_key_settings'),
            'game',
            'game_section_google_developers',
            array(
                'label_for'         => 'game_google_secret_keys',
                'class'             => 'game_row'
            )
        );
        add_settings_section('game_section_facebook_developers',
        __( 'Facebook settings', 'game' ), array($this,'game_section_facebook_developers_callback'),
            'game'
        );
        add_settings_field(
            'game_facebook_api_keys',
            __( 'Facebook Api Id:', 'game' ),
            array($this,'game_field_facebook_api_settings'),
            'game',
            'game_section_facebook_developers',
            array(
                'label_for'         => 'game_facebook_api_keys',
                'class'             => 'game_row'
            )
        );
       
    }
    public static function game_section_developers_callback( $args ) {}    
    public static function game_field_start_popup_seconds( $args ) {
        $options = get_option( 'game_options' );                
        if(!isset($options[$args['label_for']])){
            $options[ $args['label_for']]="";
        };
        ?>
        <input id="<?php echo esc_attr( $args['label_for'] ); ?>" style="width:20%" type="text" required name="game_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for'] ]; ?>" />
        <?php
    }
    public static function game_section_google_developers_callback( $args ) {}
    public static function game_field_google_api_settings( $args ) {
        $options = get_option( 'game_options' );                
        if(!isset($options[$args['label_for']])){
            $options[ $args['label_for']]="";
        };
        ?>
        <input id="<?php echo esc_attr( $args['label_for'] ); ?>" style="width:20%" type="text" required name="game_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for'] ]; ?>" />
        <?php
    }
    public static function game_field_google_secret_key_settings( $args ) {
        $options = get_option( 'game_options' );                
        if(!isset($options[$args['label_for']])){
            $options[ $args['label_for']]="";
        };
        ?>
        <input id="<?php echo esc_attr( $args['label_for'] ); ?>" style="width:20%" type="text" required name="game_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for'] ]; ?>" />
        <?php
    }
    public static function game_section_facebook_developers_callback( $args ) {}
    public static function game_field_facebook_api_settings( $args ) {
        $options = get_option( 'game_options' );                
        if(!isset($options[$args['label_for']])){
            $options[ $args['label_for']]="";
        };
        ?>
        <input id="<?php echo esc_attr( $args['label_for'] ); ?>" style="width:20%" type="text" required name="game_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for'] ]; ?>" />
        <?php
    }
    
    // Lucky Spin Settings
}
new WPLI_Admin( 'WP Game Plugin','1.0.0' );