<?php
/**
 * @link              #
 * @since             1.0.0
 * @package           WGP
 *
 * @wordpress-plugin
 * Plugin Name:       Playgret
 * Plugin URI:        #
 * Description:       This plugin is used to play game
 * Version:           1.0.0
 * Author:            Rohit Sharma
 * Author URI:        http://rohitfullstackdeveloper.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       WGP
 * Domain Path:       /languages
 */
if ( !defined( 'ABSPATH' ) ) exit;
define( 'WGP_APP_DIRNAME', basename( dirname( __FILE__ ) ) );
define( 'WGP_APP_RELPATH', basename( dirname( __FILE__ ) ) . '/' . basename( __FILE__ ) );
define( 'WGP_APP_PATH', plugin_dir_path( __FILE__ ) );
define( 'WGP_APP_URL', plugin_dir_url( __FILE__ ) );
define( 'WGP_APP_PREFIX', 'app' );
define( 'GWP_COUPON_SUCCESS', 200 );
define( 'WGP_BASE_URL' ,'http://34.93.247.101/api/');
define( 'WGP_BASE_API_URL' ,'http://34.93.247.101/');
/**
 *	@descripiton 	This function wil import the listing table on activation
 *	@param 			NONE
 *	@return 		NONE
 */

//	Include Bridge File
require WGP_APP_PATH . 'includes/class-game-plugin-importer.php';
/**
 *	@description  This function will load the bridge file for the plugin
 *	@param 			NONE
 *	@return 		NONE
 */
function run_game_plugin() {
	$plugin = new game_plugin_importer();
}
// Call Importer Function
run_game_plugin();