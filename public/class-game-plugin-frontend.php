<?php
/**
 * The frontend-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the frontend-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */
class TTE_Frontend {
    private $plugin_name,$helper,$version;
	public $applied_coupons = array();
    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
		$this->helper = new game_plugin_helper();
        $this->version = $version;
		// echo '<pre>';print_r($_SESSION);exit;
        add_action( 'wp_enqueue_scripts', array($this,'wgp_frontend_styles' ));
        add_shortcode( 'game_popup_settings', array($this,'wgp_game_popup_registration' ));
		add_action( 'wp_ajax_nopriv_get_gameListingfields',  array( $this,'gameListingfieldshandler' ));
        add_action( 'wp_ajax_get_gameListingfields',  array( $this,'gameListingfieldshandler' ));
		add_action( 'wp_ajax_nopriv_gameListingcouponcodeelement',  array( $this,'gameListingcouponcodeelementhandler' ));
        add_action( 'wp_ajax_gameListingcouponcodeelement',  array( $this,'gameListingcouponcodeelementhandler' ));
		add_action( 'wp_ajax_nopriv_gameListingcouponpointselement',  array( $this,'claimCoupon' ));
        add_action( 'wp_ajax_gameListingcouponpointselement',  array( $this,'claimCoupon' ));
		add_action( 'wp_ajax_nopriv_gameListingcouponcodeelementcart',  array( $this,'gameListingcouponcodeelementcarthandler' ));
        add_action( 'wp_ajax_gameListingcouponcodeelementcart',  array( $this,'gameListingcouponcodeelementcarthandler' ));
		add_filter( 'wp_nav_menu_items', array($this,'menus_defined_logout'), 10, 1 );
		add_filter( 'wp_logout', array($this,'auto_redirect_after_logout'), 10, 2 );
		add_action('init', array($this,'wgp_game_session' ), 1);
		add_action('init', array($this,'app_output_buffer'));
		add_action("template_redirect", array($this,'playgret_dashboard_redirect'));
		add_action( 'woocommerce_thankyou', array($this,'action_woocommerce_thankyou'), 10, 6 ); 
		add_action('wp_footer',array($this,'wgpheaderhandler'));
    }

	public function auto_redirect_after_logout(){
		session_start();
		session_destroy();
		wp_redirect( home_url() );
	}

	public function wgp_game_session(){
		if (!session_id())
		session_start();
	}

	public function claimCoupon(){
		$getuserApi = get_option('user_api_detail');
		$c = new WC_Coupon($_REQUEST['coupon_code_element']);
		$date_expires = $c->get_date_expires();
		$expirydate = $date_expires->date('Y-m-d');
		$coupon_id =  $c->id;
		
		$url = WGP_BASE_URL.'coupons/claim_coupon/';
		
		$current_user = wp_get_current_user();
		$coupon_array =  array(
			'coupon_id' => $c->id,
			'coupon_code' => $_REQUEST['coupon_code_element'] ? $_REQUEST['coupon_code_element'] : 0,
			'coupon_valid_till' => $expirydate,
			// 'client' => 'OlaPlex',
			'client' => $_REQUEST['user_name'],
			'ecom_platform' => 'WP',
			'points' => $_REQUEST['couponpoints'] ? (int)$_REQUEST['couponpoints'] : 0,
		);
		// echo "<pre>";print_r($coupon_array);exit;
		$coupon_data = json_encode($coupon_array);
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>$coupon_data,
        CURLOPT_HTTPHEADER => array(
            'Authorization:token'.' '.$getuserApi,
            'Content-Type: application/json',
            'accept:  application/json, text/javascript, */*; q=0.01',
            'accept-encoding: en-US,en;q=0.9',
            'origin:'.WGP_BASE_API_URL,
            'pragma: no-cache'
        ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
		$final_response = json_decode($response);
		// echo "<pre>";print_r($response);exit;
		$response_array = array();
		if($final_response->success == 0 || $final_response->success == ''){
			if($final_response->error == ''){
				$final_response->error =  'error';
			}
			$response_array = array(
				'success' => 0,
				'message' => $final_response->error,
			);
		}
		echo json_encode($response_array);exit;
		/*$jsonResponseclaimCouponData = json_decode($response); 	
		if($jsonResponseclaimCouponData->success == true){
			return $jsonResponseclaimCouponData->points;
		}elseif($jsonResponseclaimCouponData->success == false){
			return $jsonResponseclaimCouponData->points;
		}else{
			return false;
		}*/
	}
	public function app_output_buffer() {
		ob_start();		
		
	}
	public function wgpheaderhandler() {
		// echo '<pre>';print_r($_SESSION);exit;
		// echo wp_logout_url( esc_url(home_url('/')) ); exit;
	?>
		<div class="sidebar-background-second ">
			<span id="play-win"><?php echo __('PLAY AND WIN','GAME-PLUGIN'); ?></span> 
		</div>
		<input type="hidden" id="couponpoints">
		<input type="hidden" id="google_name">
		<input type="hidden" id="google_email">
		<input type="hidden" id="google_id">
		<input type="hidden" id="facebook_name">
		<input type="hidden" id="facebook_email">
		<input type="hidden" id="facebook_id">
		<?php 
		// if(empty($_SESSION['facebook_id']) && empty($_SESSION['google_id'])) 
		// {
		?>
		<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content px-md-4 p-sm-3 p-4">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					<div class="image"> 
						<img src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/controller-image.jpg'?>" class="text-center">
					</div>
					<p class="r3 px-sm-1"><?php echo __('Play games with us and win rewards!','GAME-PLUGIN'); ?></p>

					<div class="text-center mb-3 text-button start">
						<a id="reach-you-form-Modal-start"><?php echo __('START','GAME-PLUGIN'); ?>
							<img src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/icon.jpg'?>">
						</a>
					</div>
					
				</div>
			</div>
		</div>
		<?php 
		// Get Facebook App id
		$options = get_option( 'game_options' );
		$game_google_api_keys = $options['game_google_api_keys']; ?>
		<?php $reach_you = 'reach-you-form-Modal'?>
		<script src="http://apis.google.com/js/client:platform.js?onload=renderButton" async defer></script>
		<meta name="google-signin-client_id" content="<?php echo $game_google_api_keys; ?>">
		<div class="modal fade <?php echo $reach_you;?>" id="reach-you-form-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle"></h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body reach-text" id="text-upper">
						<div class="upper-text">
							<a data-toggle="modal" data-target="#register-form-Modal" id="to-recover" class="upper-text"><?php echo __('How can we reach you?','GAME-PLUGIN'); ?></a>
						</div>
						<div class="bottom-text">
							<a data-toggle="modal" data-target="#wgp-game-form" id="register-form" class="register-text"><?php echo __('Show registration option via ','GAME-PLUGIN'); ?>
								<?php //echo $google_url.'/'.$fb_login;	?>
								<div id="gSignIn" class="g-login" style="text-align: center;margin-left: 118px;"></div>
								<!-- Facebook login or logout button -->
								<a href="javascript:void(0);" onclick="fbLogin();" id="fbLink"><img src="<?php echo WGP_APP_URL ?>/public/assets/images/fb-login-btn.png" style="width: 245px;"/></a>

								<div class="ac-data" id="userData"></div>

								<?php /*if(empty($_SESSION['access_token'])){
									echo $google_url;	
								}else{ ?>
									<a data-toggle="modal" data-target="#wgp-game-form" id="wgp-game-form-modal" class="upper-text">Google</a>
								<?php } 
								
								if($fb_login){
									echo $fb_login; 
								}else{
								?>
									<a data-toggle="modal" data-target="#wgp-game-form" id="to-recover" class="upper-text">Fb</a>
								<?php }*/ ?>
							</a>
						</div>
					</div>
					<div class="modal-footer">
						<a data-toggle="modal" id="wgp-signup-form" class="register-text"><?php echo __('Sign Up','GAME-PLUGIN'); ?></a>
					</div>
				</div>
			</div>
		</div>	
		<div class="modal fade" id="wgp-game-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalCenterTitle"></h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body reach-text" id="text-upper">
						<div class="upper-text">
							<h3 id="to-recover" class="upper-text"><?php echo __('Register Here','GAME-PLUGIN'); ?></h3>
						</div>
						<div class="form-register">
							<form class="wgp_userform">
								<div class="form-group">	
									<?php 
										if($_SESSION['facebook_name']){
											$firstName = $_SESSION['facebook_name'];
										}else{
											$firstName = $_SESSION['google_name'];
										}
									?>					
									<input type="text" class="form-control" id="exampleInputfirstname1" value ="<?php echo $_SESSION['firstName'];?>" placeholder="First-Name">
									<input type="hidden" class="form-control" id="exampleInputgoogleid" value ="<?php echo $_SESSION['google_id'];?>">
									<input type="hidden" class="form-control" id="exampleInputfacebookid" value ="<?php echo $_SESSION['facebook_id'];?>">
								</div>
								<div class="form-group">						
									<input type="text" class="form-control" id="exampleInputlastname1" value ="" placeholder="Last-Name">
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="exampleInputEmail1" value ="" placeholder="Email">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
								</div>
								<div class="form-group">
									<input type="password" class="form-control" id="exampleInputConfirmPassword1" placeholder="Confirm-Password">
								</div>
								<input id="wheel-submit" class="btn btn-primary btn-submit" value="Submit">
							</form>
						</div>
					</div>	     
				</div>
			</div>
		</div>
		<?php 
		// } else {
		?>	
		<div class="modal fade" id="reach-you-wheel-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelNextTime" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-body reach-text" id="text-upper">
						<div class="coupon" id="h4">
							<div class="general">
								<div class="bgMaindiv">
									<div class="logo">
										<img src="<?php echo WGP_APP_URL.'public/assets/images/logo.png';?>" alt="logo">
									</div>
									<div id="main">
										<div class="spin_wheel_section">
											<div class="spin_pin"></div>
											<div width="438" height="582" class="the_wheel" align="center" valign="center">
												<div class="wheel_inner">
													<canvas id="canvas" width="434" height="434">
														<p style="color: white" align="center"><?php echo __('Sorry, your browser doesnt support canvas. Please try again.','GAME-PLUGIN'); ?></p>
													</canvas>
												</div>
											</div>
										</div>
										<div class="spin_speed_select_section">
											<div >
												<div class="main_sec">
													<!--<h2 id="spinWinResult" class="title"> </h2>-->
													<input type="button" value="Reset" class="reset_btn" style="display:none;">
												</div>
												<div class="power_controls" style="display:none;">
													<div class="main_sec">
														<h2 class="title"></h2> 
													</div>
													<div class="power">
														<div class="spin_speed">
															<h4><?php echo __('Select spinning wheel speed','GAME-PLUGIN'); ?></h4>
															<div class="spin_type">
																<a href="javascript:void(0);" id="lowspeed" class="lowspeed" onClick="selectedSpeed(1);"><?php echo __('Low','GAME-PLUGIN'); ?></a>
																<a href="javascript:void(0);" id="mediumspeed" class="mediumspeed" onClick="selectedSpeed(2);"><?php echo __('Medium','GAME-PLUGIN'); ?></a>
																<a href="javascript:void(0);" id="highspeed" class="highspeed" onClick="selectedSpeed(3);"><?php echo __('High','GAME-PLUGIN'); ?></a>
															</div>
														</div>
														<div class="spin_now_btn">
															<a id="spin_button" href="javascript:void(0);"><?php echo __('Spin Now!','GAME-PLUGIN'); ?></a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div id="spin_again" class="modal fade spin_result" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <button type="button" class="close" data-bs-dismiss="modal">&times;</button>
                    <div class="modal-body">
                        <p id="displayprice"></p>
                    </div>
                </div>
            </div>
        </div>
		<div id="spin" class="modal fade spin_result self-movtivation" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<button type="button" class="close" data-bs-dismiss="modal">&times;</button>
					<div class="modal-body reach-text" id="text-upper">
						<!-- <p id="displayprice"></p> -->
						<div class="reward-image">
							<span class="reward-image1">	
								<img src="<?php echo WGP_APP_URL. 'public/assets/images/reward-box.png'?>">
							</span>
	        			</div>
						<div class="reward-button">
							<button type ="button" id="reward-coupon-Modal" class="btn btn-primary"><?php echo __('CLAIM NOW','GAME-PLUGIN'); ?></button>
						</div>
					</div>
				</div>
			</div>
    	</div>
		
		<div class="modal fade" id="coupon-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
				<div class="modal-body reach-text" id="text-upper">
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<div class="title-text">
					<?php echo __('Choose a coupon','GAME-PLUGIN'); ?>
					</div>
					<div class="coupon-image">
					<span class="coupon-image-banner">
						<?php 
							$coupon_posts = get_posts( array(
								'posts_per_page'   => 3,
								'orderby'          => 'post_date',
								'order'            => 'desc',
								'post_type'        => 'shop_coupon',
								'post_status'      => 'publish',
							) );
							$coupon_codes = []; // Initializing
							if(!empty($coupon_posts)){
							// echo '<pre>';	print_r($coupon_posts);exit;
							?>
							<?php
								foreach($coupon_posts as $coupon_post){							
										$coupon_codes = $coupon_post->post_title;
										$c = new WC_Coupon($coupon_codes);
										// echo '<pre>';	print_r($c);exit;
										?>
										<input type="text" value="<?php echo $coupon_codes?>" coupon_id="<?php echo $c->id;?>" id="copytext<?php echo $c->code?>" class="form-control copycode "readonly>
										<div class="coupon_codes hidden"><?php echo $c->code?></div>
										<div class="coupon-c-codes p-3">
											<div class="card-cont pull-left">
												<h6 class="d-block">Enjoy <?php echo $c->amount;?>% OFF</h6>
												<small class="text-black-50"><?php echo __('on all night creams!','GAME-PLUGIN'); ?></small>
												<h6 class="d-block color-100"> 
													<span class="users_count"><?php echo __('100','GAME-PLUGIN'); ?> </span>
													<span class="text-black-50"><?php echo __('users already brought!','GAME-PLUGIN'); ?></span>
												</h6>
												<h6 class="d-block"><?php echo __('40 XP','GAME-PLUGIN'); ?></h6>
											</div>
											<a id ="<?php echo $c->id;?>" onclick="copycouponFunction('copytext<?php echo $c->code?>')" class="pull-right couponmaker"><span><?php echo __('Click me','GAME-PLUGIN'); ?><br/><?php echo __('to copy code','GAME-PLUGIN'); ?></span></a>
										</div>	
								<?php }
							}
						?>			  	
					</span>
					</div>  
					
					<div class="wrapper" data-anim="base wrapper">
						<div class="card">
							<div class="circle">
								<div class="bar"></div>
								<div class="box"><span>00</span></div>
							</div>
							<div class="text">HOURS</div>
						</div>
						<div class="card">
							<div class="circle">
								<div class="bar"></div>
								<div class="box"><span>00</span></div>
							</div>
							<div class="text">MINUTES</div>
						</div>
						<div class="card">
							<div class="circle" data-anim="base left">
								<div class="bar"></div>
								<div class="box"><span id="Timer">30</span></div>
							</div>
							<div class="text">SECONDS</div>
						</div>
					</div>
					
					<div class="under-text">
						<?php echo __('Time remaining to redeem your coupon','GAME-PLUGIN'); ?>
					</div>
					<div class="under-text2">
						<?php echo __('Redeem coupons at our online store','GAME-PLUGIN'); ?>
					</div>
				</div>
				
				</div>
			</div>
		</div>
		<div class="modal fade" id="earn-points-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-body reach-text" id="text-upper-earn-point">
						<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
						<div class="reward-image">
							<?php 
								// $claimCoupon = $this->helper->claimCoupon(); 
								$claimCoupon = 0;
							?>
							<h4><?php echo __('Wow! You just earned','GAME-PLUGIN'); ?></h4>
							<div class="reward-points">
								<span class="rewards "><?php echo $claimCoupon ;?></span>
								<img src="<?php echo WGP_APP_URL. 'public/assets/images/xp-point.png'?>">
							</div>
							<p><?php echo __('Get more points when you redeem your coupons','GAME-PLUGIN'); ?></p>
							<!--<span class="reward-image1">
								<img src="<?php echo WGP_APP_URL. 'public/assets/images/earn-banner.jpg'?>">
							</span>-->
						</div>
						<div class="reward-button">
							<button id="level-Modal" class="btn btn-primary"><?php echo __('Check Your Performance','GAME-PLUGIN'); ?></button>
						</div>
					</div>	      
				</div>
			</div>
		</div>
		<div class="modal fade" id="level-Modal-cong" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">	      
				<div class="modal-body reach-text" id="text-upper-congrutes">
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<div class="level-image card">
					<span class="reward-image-congrutes">
						<img src="<?php echo WGP_APP_URL. 'public/assets/images/level2.png'?>">
					</span>
					<div class="congrats-text">
						<?php echo __('Congratulations!','GAME-PLUGIN'); ?>
					</div>
					<span class="congrats-bottom-text">
						<?php echo __('You got a level Upgrade','GAME-PLUGIN'); ?>
					</span>
					<div class="arrow-icon">
						<i class="fa fa-angle-double-up" aria-hidden="true"></i>
					</div>
					<span class="level-bottom-text">
						<?php echo $this->helper->getUserLevel();?>
					</span>
					<span id="points-Modal" class="progress-bottom-text">
						<?php echo __('Check your progress here','GAME-PLUGIN'); ?>
					</span>
					<span class="comeback-bottom-text">
						<?php echo __('Come back tomorrow to play again!','GAME-PLUGIN'); ?>
					</span>
					</div>
				</div>	      
				</div>
			</div>
		</div>
		<div class="modal fade" id="points-Modal-earn" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
				<div class="modal-body reach-text" id="text-upper-earned">
					<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
					<?php 
					$jsonResponseProfile = $this->helper->getUserprofile();	
					$xpPoints = $jsonResponseProfile->xp;
					$profileImage = $jsonResponseProfile->profile_image;
					$getProfileName = $jsonResponseProfile->name;
					$getProfileBadges = $jsonResponseProfile->badges;
					$getValuemax	 = 10;
					if(!empty($getProfileBadges)){
						foreach($getProfileBadges as $getProfileBadge){
							$getProfileBadgeIcon = $getProfileBadge->icon;
							$getProfileBadgeActive = $getProfileBadge->active;
							$getProfileBadgeName = $getProfileBadge->name;
						}
					}
					$getProfileBadgesLevel = $jsonResponseProfile->level;
					//echo'<pre>';print_r($jsonResponseProfile);exit;
					?>
					<div class="level-image">
					<span class="reward-image1">
						<?php 
							if($profileImage != '' || $profileImage != null){ ?>
								<img src="<?php echo $profileImage ?>">
						<?php
						}else{
						?>
								<img src="<?php echo WGP_APP_URL. 'public/assets/images/default-img.png'?>">
						<?php 
						}
						?>	  
					</span>
					<div class="person-text">
						<?php echo $getProfileName ?>
					</div>
					<span class="person-bottom-text">
						<?php echo __('My XP Points: '.$xpPoints.' XP','GAME-PLUGIN'); ?>
					</span>
					<div class="my-level-text">
						<div class="col-lg-12 d-flex">
							<div class="col-sm-2 d-flex" style="line-height: 14px;">
								<div class="level-text"><?php echo __('My Level :','GAME-PLUGIN'); ?></div>
							</div>
							<div class="col-lg-8">
								<div class="progress">
									<div class="progress-bar" role="progressbar" style="width: <?php echo $getProfileBadgesLevel * $getValuemax ?>%" aria-valuenow="15" aria-valuemin="0" aria-valuemax="<?php echo $getValuemax * 10 ?>"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="my-level-text">
						<div class="col-lg-12 d-flex">
							<div class="col-lg-3 d-flex pl-0" style="line-height: 40px;">
								<div class="level-text"><?php echo __('My Badges :','GAME-PLUGIN'); ?></div>
							</div>
							<div class="col-lg-8 progressBarUser">
								<div class="badges-point-img">
									<img src="<?php echo $getProfileBadgeIcon;?>">
								</div>
							</div>
						</div>
					</div>
					<div class=points-main-text>
					<span class="points-bottom-text">
						<?php echo __('Check Your Coupons','GAME-PLUGIN'); ?>
					</span>
					</div>
					<div class="points-button">
						<button id="level-Modal-Leaderboard" class="btn btn-primary"style="padding: 13px 60px 13px 60px;background-color: #1d3557;"><?php echo __('Go To Leaderboard','GAME-PLUGIN'); ?></button>
					</div>
				</div>
				</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="level-Modal-Leaderdashboard" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-body" id="text-upperLeaderBoard">
						<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="container dashboard-bg">
						<div class="col-12 leader-title pr-0 pl-0">
							<span>LEADERBOARD</span>
						</div>
						
						<?php $jsonResponseLeaderBoardData = $this->helper->getLeaderBoardData();	
							// echo "<pre>";print_r($jsonResponseLeaderBoardData);exit;					  	
							if(!empty($jsonResponseLeaderBoardData->profiles)){ ?>	
								<div class="row profile-main">						
									<?php 
										$count = 1;
										foreach($jsonResponseLeaderBoardData->profiles as $key => $leaderboardData){
											
											if($key < 3){
												$div2_class = $key % 2 == 0 ? 'leaderboarddatacustom' : '';
											?>
											<div class="col-sm-4 main-box <?php echo $div2_class;?>">
												<div class="card profile-box">
													<div class="card-body p-0">
														<div class="w-100 d-flex medal-img">
															<div class="col-sm-5 pr-0">
																<img class="left-medal" src="<?php echo WGP_APP_URL. 'public/assets/images/first-medal.jpg'?>">
																<div class="text-pts">
																	<?php echo $leaderboardData->xp?> pts
																</div>
															</div>
															<div class="col-sm-7 profile-person pr-0">
															<?php 
																//echo "<pre>";print_r($leaderboardData->profile_image);exit;
																if($leaderboardData->profile_image != '' || $leaderboardData->profile_image != null){
																?>
																	<img class="left-medal" src="<?php echo $leaderboardData->profile_image?>">
																<?php
																}else{
																?>
																	<img class="left-medal" src="<?php echo WGP_APP_URL. 'public/assets/images/default-img.png'?>">
																<?php 
																}
															?>
																
															</div>
														</div>
														<div class="w-100 description">
															<h5 class="card-title pt-3"><?php echo $leaderboardData->name?></h5>
														</div>
														<div class="w-100 description2">
															<h5 class="card-title pt-2">New Face</h5>
														</div>
														<div class="w-100 d-flex achieve-front-text">
															<div class="col-sm-6 pr-0">
																<span class="text-achieve">
																	Achievements:
																</span>
															</div>
															<div class="col-sm-6 medal-person pl-0 pr-0">
															<?php 
																$getLeaderBadges = $leaderboardData->badges;
																//echo "<pre>";print_r($getLeaderBadges);exit;
																if(!empty($getLeaderBadges)){
																	foreach($getLeaderBadges as $getLeaderBadges){ ?>
																		<img class="img-medal" src="<?php echo $getLeaderBadges->icon;?>">
																	<?php }
																}
															?> 
															</div>
														</div>
													</div>
												</div>
											</div>
										<?php 
										}else{
									?>
								</div>
								<div class="row profile-lower">
									<div class="col-sm-12 lower-box" style="padding-right: 80px;padding-left: 80px; padding-top:20px">
										<div class="card profile-box2 p-4">
											<div class="card-body p-0">
												<div class="w-100 d-flex medal-img">
													<div class="col-sm-1 pr-0 circle-height">
														<div class="circle-num">
															<span><?php echo $count;?></span>
														</div>
													</div>
													<div class="col-sm-3 botoom-profile-person pr-0">
													<?php 
														if($leaderboardData->profile_image != '' || $leaderboardData->profile_image != null){
													?>
														<img class="left-medal" src="<?php echo $leaderboardData->profile_image?>">
														<?php
														}else{
														?>
														<img class="left-medal" src="<?php echo WGP_APP_URL. 'public/assets/images/default-img.png'?>">
														<?php } ?>
														<span><?php echo $leaderboardData->name?></span>
													</div>
													<div class="col-sm-2 botoom-pts pr-0">
														<span><?php echo $leaderboardData->xp?></span>
													</div>
													<div class="col-sm-2 botoom-face pr-0">
														<span>New-Face</span>
													</div>
													<div class="col-sm-4 botoom-award pr-0">
														<?php 
															$getLeaderBadges = $leaderboardData->badges;
															//echo "<pre>";print_r($getLeaderBadges);exit;
															if(!empty($getLeaderBadges)){
																foreach($getLeaderBadges as $getLeaderBadges){ ?>
																	<img class="bottom-medal" src="<?php echo $getLeaderBadges->icon;?>">
																<?php }
															}
														?> 
													</div>
												</div>
											</div>
										</div>
									</div>
								<?php
									}
									$count++;
								}
								?>
								</div>
								<?php							
							}
						?>
					</div>
				</div>
			</div>
		</div>	
	<?php 
	// }
		$popup_second = get_option('game_options');
	?>
	<script>
	jQuery( document ).ready(function($){
		var second = '<?php echo $popup_second['game_start_popup_seconds'];?>';
		var finaltime=  parseInt(second)*1000;
		var administrator = '<?php echo current_user_can( 'administrator' ); ?>';	
		var timer; 
		facebook_id = Cookies.get('facebook_id');
		google_id = Cookies.get('google_id');
		// console.log('facebook_id ' + facebook_id);
		// console.log('google_id ' + google_id);
		// $('#form').modal('show');
		// if(getCookie("the_cookie") == '' && administrator == ''){
			// var wheel_auto_open = '';
			if(!Cookies.get('wheel_auto_open')){
				console.log('wheel_auto_open ' + Cookies.get('wheel_auto_open'));
			}else{
				console.log('wheel_auto_open ' + Cookies.get('wheel_auto_open'));
			}
			setTimeout(function () {
				console.log('facebook_id ' + facebook_id);
				console.log('google_id ' + google_id);
				console.log('isloginstatus ' + Cookies.get('isloginstatus'));
				if(Cookies.get('wheel_auto_open') != 'false'){
					if(Cookies.get('isloginstatus') == 1){
						console.log('wheel modal');
						$('#reach-you-wheel-Modal').modal('show');
						$('#form').modal('hide');
						$('#reach-you-form-Modal').modal('hide');       
						console.log(true);
					}else{
						console.log('form modal');
						timer=$('#form').modal('show');
						console.log(false);
					}
				}
			}, finaltime);
		
		/*if( googlecode !=""){
			$('#reach-you-form-Modal').modal('show');
			$('#form').modal('hide');
		}else{
			var timer; 
			if(getCookie("the_cookie") == '' && administrator == ''){
				setTimeout(function () {
					// timer=$('#form').modal('show');
				}, finaltime);
			}	
		}*/


	});
	</script>
	<?php
	}

	public function action_woocommerce_thankyou($order_id){
		add_option('gameaction', 'Thankyou page landing---order id is:'. $order_id, 'yes');
		// echo "<pre>";print_r($order_get_id);
		// exit; ?>
		<div class="modal fade" id="earn-points-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<?php $redeemCoupon = $this->helper->redeemCoupon($order_id); 
					?>
					<div class="modal-body reach-text" id="text-upper-earn-point">
						<button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
						<h4><?php echo __('Wow! You just earned','GAME-PLUGIN'); ?></h4>
						<div class="reward-points">
							<span class="rewards "><?php echo $redeemCoupon ;?></span>
							<img src="<?php echo WGP_APP_URL. 'public/assets/images/xp-point.png'?>">
						</div>
						<p><?php echo __('Get more points when you redeem your coupons','GAME-PLUGIN'); ?></p>
						<!--<span class="reward-image1">
							<img src="<?php echo WGP_APP_URL. 'public/assets/images/earn-banner.jpg'?>">
						</span>-->
						<div class="reward-button">
							<button id="level-Modal" class="btn btn-primary"><?php echo __('Check Your Performance','GAME-PLUGIN'); ?></button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			jQuery( document ).ready(function(){
				$('#earn-points-Modal').modal('show');
				// Cookies.set('wheel_auto_open', 'undefined');
			})
		</script>
	<?php }

	public function has_discount( $coupon_code = '' ) {	
		// echo '<pre>'; print_r( WC()->session->get( 'applied_coupons', array() ) );exit;
		return $coupon_code ? in_array( wc_format_coupon_code( $coupon_code ), WC()->session->get( 'applied_coupons', array() ), true ) : count( WC()->session->get( 'applied_coupons', array() ) ) > 0;
	}
	
	public function gameListingcouponcodeelementcarthandler(){
		// WC()->session->set( 'applied_coupons', null );
		// echo '<pre>'; print_r( WC()->session->get( 'applied_coupons', array() ) );exit;
		global $woocommerce;
		$cart = WC()->cart;

		//echo "<pre>";print_r($cart);exit;
		$coupon_code = $_REQUEST['coupon_code_front'];	
		// $cart->add_discount( $coupon_code );
		$current_coupon = WC()->cart->get_coupons();
		// $this->applied_coupons = $coupon_code;
		$user = wp_get_current_user();
		$user_id = $user->ID;
		$userCoupons = get_user_meta($user_id,'_coupon_code');
		// echo '<pre>';print_r($userCoupons); exit;
		$error = array();
		if(empty($_POST['coupon_code_front'])){		
			$error['coupon_code_front'] = 'Coupon Code Should not be blank';
		}
		if(!empty($userCoupons)){
			if(!in_array($coupon_code, $userCoupons)){
				$error['coupon_code_not_exist'] = 'You can not use this Coupon Code. Please claim it firstly';
			}else{
				if(empty($current_coupon)):
					WC()->cart->apply_coupon( $coupon_code );
					$error['coupon_code_applied'] = 'Coupon code applied successfully.';
				elseif(in_array($coupon_code , WC()->session->get( 'applied_coupons', array() ))):
					$error['coupon_code_already'] = 'Coupon Already Applied.';
				else:
					WC()->cart->apply_coupon( $coupon_code );
					$error['coupon_code_applied'] = 'Coupon code applied successfully.';
				endif;
			}
		}
		
		$response['success'] = 0;
        $response['message'] = $error;
		
		if(!$error){
			$success = GWP_COUPON_SUCCESS;
			WC()->cart->apply_coupon( $coupon_code );
			do_action( 'woocommerce_applied_coupon', $coupon_code );
			$response['success'] = 1;
			$response['message'] = $success;
			$response['applied_session'] = WC()->session->get( 'applied_coupons', array() );
		}
        echo json_encode($response); exit;
	}

	public function gameListingcouponcodeelementhandler(){
		if(!empty($_SESSION['google_email'])){
			$useremail = $_SESSION['google_email'];
		}elseif(!empty($_SESSION['facebook_email'])){
			$useremail = $_SESSION['facebook_email'];
		}else{
			$useremail = '';
		}
		$getUserEmail = get_user_by('email',$useremail);		
		$coupon_code = $_REQUEST['coupon_code_element'];		
		$user = wp_get_current_user();
		if($getUserEmail->roles[0] == 'subscriber'){
			$user_id = $getUserEmail->data->ID;
		}else{
			$user_id = $user->ID;
		}
		$result = add_user_meta( $user_id, '_coupon_code', $coupon_code);
		$response['success'] = 1;
		$response['message'] = 'Coupon Code Added Succesfully';
		$response['user_id'] = $user_id;
		$response['coupon-code'] = $coupon_code;	
        echo json_encode($response); exit;
	}

	public function playgret_dashboard_redirect(){
		global $post;
		if($post->post_name == 'leader-dashboard'){
			if ( file_exists( WGP_APP_PATH . 'includes/CPT/leader-dashboard.php' ) ) {
				$return_template = WGP_APP_PATH . 'includes/CPT/leader-dashboard.php';
			}
			$this->do_playgret_redirect($return_template);
		}
	}
	public function do_playgret_redirect($url) {
		global $post, $wp_query;
		if (have_posts()) {
			include($url);
			die();
		} else {
			$wp_query->is_404 = true;
		}
	}
	public function gameListingfieldshandler(){
		// global $wp_session;
		session_start();

		// Insert Token
		if(!empty($_REQUEST['email'])){
			$urlApi = WGP_BASE_URL.'profile/get_user_token/';
			//$email  = $_SESSION['email'];
			$request_array = array(
				'email' 				=> $_REQUEST['email']
			);		
			//echo "<pre>";print_r($request_array);exit;
			$api_response = wp_remote_post( $urlApi, array(
				'method'      			=> 'POST',
				'headers'       		=>array('Content-Type' => 'application/json'),
				'body'       			=> json_encode($request_array)
				)
			);		
			$json_response = json_decode($api_response['body']);
			
			$token = $json_response->token;			
			$getuserApiToken = get_option('user_api_detail');
			if(false !== $getuserApiToken){
				if($getuserApiToken === $token){
	
				}else{
					update_option('user_api_detail',$token,'yes');
				}
			}else{
				add_option('user_api_detail',$token);
			}
		}
		
		
		if($_REQUEST['type'] == 'google'){
			$_SESSION['google_name'] = $_REQUEST['name'];
			$_SESSION['google_email'] = $_REQUEST['email'];
			$_SESSION['google_id'] = $_REQUEST['google_id'];
			$_SESSION['type'] = $_REQUEST['type'];
		}elseif($_REQUEST['type'] = 'facebook'){
			$_SESSION['facebook_name'] = $_REQUEST['name'];
			$_SESSION['facebook_email'] = $_REQUEST['email'];
			$_SESSION['facebook_id'] = $_REQUEST['facebook_id'];
			$_SESSION['type'] = $_REQUEST['type'];
		}
		
		if($_REQUEST['type'] == 'google' || $_REQUEST['type'] == 'facebook'){
			$user = get_user_by( 'email', $_REQUEST['email'] );
			// echo'<pre>';print_r($user);exit;
			if( empty( $user->data )){
				$userData = array(
					'user_login'=>$_REQUEST['name'],
					'user_email'=> $_REQUEST['email'],
					'user_pass'=> wp_generate_password(),
					'display_name'=>$_REQUEST['first_name']
				);
				// echo'<pre>';print_r($userData);exit;
				$user_id = wp_insert_user( $userData );

				wp_set_current_user( $user_id, $_REQUEST['email'] );
				wp_set_auth_cookie( $user_id );
				do_action( 'wp_login', $_REQUEST['email'] );
				
				$response   =   array(
					'success' => 1,
					'message' => 'User Inserted Successfully!'
				);
			}else{
				$user_id = $user->data->ID;
				$user_email = $user->data->user_email;
				// echo'<pre>';
				// print_r($user_id);
				// print_r($user_email);
				// exit;
				wp_set_current_user( $user_id, $user_email );
				wp_set_auth_cookie( $user_id );
				do_action( 'wp_login', $user_email );
				
				$response = array(
					'success' => 0,
					'user_id' => $user_id,
					'user_email' => $user_email,
					'message' => 'Alredy Login'
				);
			}
			echo json_encode( $response );
		}else{
			// Manual User
			if(empty($_REQUEST['first_name']) || empty($_REQUEST['last_name']) || empty($_REQUEST['email']) || empty ($_REQUEST['password']) || empty ($_REQUEST['confirm_password'])){
				$users = get_users();
				if(empty($_REQUEST['first_name'])){
					$error['first_name'] = 'First Name is required';
				}
				if(empty($_REQUEST['last_name'])){
					$error['last_name'] = 'Last Name is required';
				}
				if(empty($_REQUEST['email'])){
					$error['email'] = 'Email is required';
				}else{
					foreach($users as $user){
						if($_REQUEST['email'] == $user->data->user_email){
							$error['email'] = 'Email is already exists';	
						}
					}
				}
				if(empty($_REQUEST['password'])){
					$error['password'] = 'Password is required';
				}
				if($_REQUEST['confirm_password'] != $_REQUEST['password']){
					$error['confirm_password'] = 'Confirm Password is not matched';
				}
				$response   =   array(
					'user_id' => array(),
					'success' => 0,
					'errors' => $error
				);
				echo json_encode( $response );
			}else{
				$lineData = array(
					'user_login'=>$_REQUEST['first_name'] .' '. $_REQUEST['last_name'],
					'user_email'=> $_REQUEST['email'],
					'user_pass'=>$_REQUEST['password'],
					'display_name'=>$_REQUEST['first_name'] .' '. $_REQUEST['last_name']
				);
				// echo '<pre>';print_r(wp_insert_user( $lineData ));exit;
				wp_insert_user( $lineData );
				
				$_SESSION['userData'] = $lineData;
				$response   =   array(
					'success' => 1,
					'message' => 'User Inserted Successfully!'
				);
				echo json_encode( $response );
			}
		}
	}
   
    public function wgp_frontend_styles(){
		global $wpdb;
    	wp_deregister_script('jquery');
    	wp_enqueue_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
		wp_enqueue_style('wgp-frontend-boostrap4.3', WGP_APP_URL.'public/assets/css/bootstrap.min.css', array(), false, false);
		//wp_enqueue_style('wgp-frontend-font-awesome4.0', APP_URL.'public/assets/css/bootstrap.min.css', array(), false, false);
    	wp_enqueue_style('wgp-frontend', WGP_APP_URL.'public/assets/css/frontend_css.css', array(), false, false);    	
    	wp_enqueue_style('wgp-luckySpin', WGP_APP_URL.'public/assets/css/luckySpin.css', array(), false, false);    	
		wp_enqueue_script( 'wgp-boostrap-bundle4.6-js',WGP_APP_URL.'public/assets/js/bootstrap.bundle.min.js');	
		wp_enqueue_script( 'wgp-boostrap-popper5.0-js',WGP_APP_URL.'public/assets/js/popper.min.js');
		wp_enqueue_script( 'wgp-boostrap-5.0-js',WGP_APP_URL.'public/assets/js/bootstrap.min.js');
		wp_enqueue_style('wgp-circleprogresscss', WGP_APP_URL.'public/assets/css/circle.css', array(), false, false); 
		wp_enqueue_script('wgp-circleprogressjs', WGP_APP_URL.'public/assets/js/circle.js');	
    	wp_enqueue_script( 'wgp-TweenMax-script', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.7.1/gsap.min.js',array(), false, false);
		wp_enqueue_script( 'wgp-luckywheel-js',WGP_APP_URL.'public/assets/js/jQuery.luckywheel.min.js',array(), false, false);
		wp_enqueue_script( 'wgp-custom-script-luckywheel-js',WGP_APP_URL.'public/assets/js/script.js',array(), false, false);
		
		// Settin Json File & spinsoun
		$settingjsonfile = WGP_APP_URL.'public/settings.json';
		$spinsound = WGP_APP_URL.'public/spinsound.mp3';

		// Get Facebook App id
		$options = get_option( 'game_options' );
		$fb_app_id = $options['game_facebook_api_keys'];

		// Wp Logout redirect 
		$logout_url = wp_logout_url( esc_url(home_url('/')) );

		wp_localize_script( 'wgp-custom-script-luckywheel-js', 'wgp_script_object',  array('settingjsonfile'=>$settingjsonfile, 'spinsound'=>$spinsound ));
    	wp_enqueue_script( 'wgp-sweetalert-js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
    	wp_enqueue_style( 'wgp-sweetalert-style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
		wp_enqueue_script( 'wgp-js-cookies',WGP_APP_URL.'public/assets/js/jquery.cookie.min.js');
    	wp_enqueue_script( 'wgp-custom-js',WGP_APP_URL.'public/assets/js/gret-game.js');
		wp_localize_script( 'wgp-custom-js', 'wgp_facebook_object',  array('fecebook_setting'=> $fb_app_id, 'logout_url' => $logout_url));
		wp_enqueue_style('wgp-frontendselect2.0', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css');		
        wp_enqueue_script( 'wgp-frontendselect-js-2.0', 'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js');
    	wp_localize_script( 'wgp-custom-js', 'wgp_ajax_object',  array('ajaxurl' => admin_url( 'admin-ajax.php' )));
    }

	public function menus_defined_logout(){
		// session_start();
		// echo'<pre>';print_r($_SESSION);exit;
		if(isset($_SESSION['google_id'])){
			$user_name = 'Hi ' .$_SESSION['google_name'];
			?>
				<div class="lp-join-now" style="display:none;">
					<span class="user-details">
						<?php esc_html_e($user_name);?>
						<a href="<?php echo wp_logout_url( esc_url(home_url('/')) ); ?>" onclick="signOut();" class=""><?php esc_html_e('Log Out', 'GAME-PLUGIN');?></a>
					</span>	
				</div>
				<div id="userContent" class="userContent" style="display:none"></div>
			<?php
		}elseif(isset($_SESSION['facebook_id'])){
			$user_name = 'Hi ' .$_SESSION['facebook_name'];
			?>
				<div class="lp-join-now">
					<span class="user-details">
						<?php esc_html_e($user_name);?>
						<a href="<?php echo wp_logout_url( esc_url(home_url('/')) ); ?>" class="" onclick="fb_Logout();"><?php esc_html_e('Log Out', 'GAME-PLUGIN');?></a>
					</span>	
				</div>
				
			<?php
		?>	
		<?php 
		}else{
		?>
		<div id="userContent" class="userContent" style="display:none"></div>
		<?php
		?>
		<?php 
		}
		?>
		
		<?php 
	}

    public function wgp_game_popup_registration(){    	
		session_start();		
		
	return ob_get_clean();
    }
	
}



new TTE_Frontend( 'WP Game Plugin','1.0.0' );