jQuery(document).ready(function($){
    // Spin wheel settings
    $('#couponcode').select2({
        maximumSelectionLength: 3
    });
    $('#spinWheel_settings').on('submit',function(e){
        e.preventDefault();
        var form_data = new FormData();
        form_data.append('action', 'playgret_spin_wheel_settingform');
        form_data.append('lpNonce', jQuery('#lpNonce').val());
        //form_data.append('userID', jQuery('#currentuserId').val());
        form_data.append('numberofsegments', jQuery('#numberofsegments').val());
        form_data.append('outerradius', jQuery('#outerradius').val());
        form_data.append('innerradius', jQuery('#innerradius').val());
        form_data.append('textfontsize', jQuery('#textfontsize').val());
        form_data.append('textmargin', jQuery('#textmargin').val());
        form_data.append('whitetextcolor', jQuery('#whitetextcolor').val());
        form_data.append('whitestrokecolor', jQuery('#whitestrokecolor').val());
        form_data.append('wheelstrokewidth', jQuery('#wheelstrokewidth').val());
        form_data.append('gameovertext', jQuery('#gameovertext').val());
        form_data.append('introtext', jQuery('#introtext').val());
        form_data.append('countdowntime', jQuery('#countdowntime').val());
        form_data.append('position', jQuery('#position').val());
        form_data.append('desktopintervaltext', jQuery('#desktopintervaltext').val());
        form_data.append('mobileintervaltext', jQuery('#mobileintervaltext').val());
        form_data.append('cookiedays', jQuery('#cookiedays').val());
        var totalfiles = document.getElementById('files').files.length;
        for(var index = 0;index < totalfiles;index++){
            form_data.append('files',document.getElementById('files').files[index]);
        }
        var markericontotalfiles = document.getElementById('markericon').files.length; 
        for(var index = 0;index < markericontotalfiles;index++){
            form_data.append('markericon',document.getElementById('markericon').files[index]);
        }
        var enablediscountbar = jQuery('#enablediscountbar').is(":checked");
        if(enablediscountbar == true){
            form_data.append('enablediscountbar', 1);
        }else{
            form_data.append('enablediscountbar', 0);
        }
        var showdesktop = jQuery('#showdesktop').is(":checked");
        if(showdesktop == true){
            form_data.append('showdesktop', 1);
        }else{
            form_data.append('showdesktop', 0);
        }
        var desktopintent = jQuery('#desktopintent').is(":checked");
        if(desktopintent == true){
            form_data.append('desktopintent', 1);
        }else{
            form_data.append('desktopintent', 0);
        }
        var desktopinterval = jQuery('#desktopinterval').is(":checked");
        if(desktopinterval == true){
            form_data.append('desktopinterval', 1);
        }else{
            form_data.append('desktopinterval', 0);
        }
        var showmobile = jQuery('#showmobile').is(":checked");
        if(showmobile == true){
            form_data.append('showmobile', 1);
        }else{
            form_data.append('showmobile', 0);
        }
        var mobileintent = jQuery('#mobileintent').is(":checked");
        if(mobileintent == true){
            form_data.append('mobileintent', 1);
        }else{
            form_data.append('mobileintent', 0);
        }
        var mobileinterval = jQuery('#mobileinterval').is(":checked");
        if(mobileinterval == true){
            form_data.append('mobileinterval', 1);
        }else{
            form_data.append('mobileinterval', 0);
        }
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: settings_all_ajax_object.ajaxurl,
            data: form_data,
            success: function(response) {
                //console.log(response);
                jQuery('.alert-dismissible, .text-danger').remove();
			     jQuery('.form-group').removeClass('has-error');
                 if(response.error){
                    if(response.error.files){
                        jQuery('#files').after('<div class="text-danger">' + response.error.files + '</div>');
                    }				
                    jQuery('.text-danger').parent().addClass('has-error');
                    if(response.error.markerfiles){
                        jQuery('#markericon').after('<div class="text-danger">' + response.error.markerfiles + '</div>');
                    }				
                    jQuery('.text-danger').parent().addClass('has-error');
                }
                if (response.data.success == 1) {
                    Swal.fire(
                       'Data Inserted successfully.',
                       '',
                       'success'
                   ).then(() => {
                       location.reload();
                   });
                }
            },
            processData: false,
            contentType: false,
        });
    });

    $('#spinWheel_segments').on('submit',function(e){
        e.preventDefault();
        var form_data = new FormData();
        form_data.append('action', 'playgretspinwheelsegmentsform');
        form_data.append('lpNonce', jQuery('#lpNonce').val());
        form_data.append('txtdisplaytext', jQuery('#txtdisplaytext').val());
        form_data.append('segmenttext', jQuery('#segmenttext').val());
        form_data.append('txtbackgroundcolor', jQuery('#txtbackgroundcolor').val());
        form_data.append('ddlgravity', jQuery('#ddlgravity').val());
        form_data.append('hdngravityperc', jQuery('#hdngravityperc').val());
        form_data.append('iscouponcode', jQuery('#iscouponcode').val());
        form_data.append('couponcode', jQuery('#couponcode').val());
        //console.log(form_data); return false;
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: settings_all_ajax_object.ajaxurl,
            data: form_data,
            success: function(response) {
                console.log(response);
                if (response.success == 1) {
                    Swal.fire(
                       'Data Inserted successfully.',
                       '',
                       'success'
                   ).then(() => {
                       location.reload();
                   });
                }
            },
            processData: false,
            contentType: false,
        });
    });

    $("#iscouponcode").on('change', function() {
        //console.log($(this).prop('checked'));
        if($(this).prop('checked') === true){
            //$('#iscouponcodevalue').val(true);
            $('#iscouponcode').val(1);
         }else{
            //$('#iscouponcodevalue').val(false);
            $('#iscouponcode').val(0);
         }
    });    

    $('.modal-toggle-edit').on('click',function(e){
        e.preventDefault();
        var edit_val = jQuery(this).attr('data-id');
        jQuery('#modal-edit').attr('data-id',edit_val);
        jQuery('#segment_file_button_update').attr('data-id',edit_val);
        $('#modal-edit').toggleClass('is-visible');
        jQuery.ajax({
            type: 'POST',
            data: { id:edit_val},
            url: settings_all_ajax_object.ajaxurl+'?action=segmentlistingedit',
            success: function(response) {
                var result = JSON.parse(response);
                if(result.success == 1){                
                    jQuery('#txtdisplaytextUpdate').val(result.txtdisplaytext);
                    jQuery('#segmenttextUpdate').val(result.segmenttext);
                    jQuery('#modal_stream_email').val(result.email);
                    jQuery('#txtbackgroundcolorUpdate').val(result.txtbackgroundcolor);
                    jQuery('#ddlgravityUpdate').val(result.ddlgravity);
                    jQuery('#hdngravitypercUpdate').val(result.hdngravityperc);
                    jQuery('#iscouponcodeUdpate').val(result.iscouponcode);
                    
                    if(result.iscouponcode == 1){
                        jQuery('#iscouponcodeUdpate').prop("checked", true)
                    }else{
                        jQuery('#iscouponcodeUdpate').prop("checked", false)
                    }
                    jQuery('#couponcodeUpate').val(result.couponcode);
                }else{
                    Swal.fire({
                        title: "No Data!",
                        text: "No Data. Please try again!",
                        icon: "error",
                        button: "Ok!",
                    });
                }
            }
        });

    });
    $('.modal-close').on('click',function(e){
        e.preventDefault();
        $('.modal').removeClass('is-visible');
    });
    $('#segment_file_button_update').on('click',function(e){
        e.preventDefault();
        var form_data = new FormData();

        if (jQuery("#iscouponcodeUdpate").is(":checked")) {
            var iscouponcodeUdpate = 1;
        } else {
            var iscouponcodeUdpate = 0;
        }
        console.log(jQuery('input:checkbox:checked').val());
        form_data.append('action', 'playgretspinwheelsegmentUpadateform');
        form_data.append('lpNonce', jQuery('#lpNonce').val());
        form_data.append('update_save_val', jQuery(this).attr('data-id'));
        form_data.append('txtdisplaytextUpdate', jQuery('#txtdisplaytextUpdate').val());
        form_data.append('segmenttextUpdate', jQuery('#segmenttextUpdate').val());
        form_data.append('txtbackgroundcolorUpdate', jQuery('#txtbackgroundcolorUpdate').val());
        form_data.append('ddlgravityUpdate', jQuery('#ddlgravityUpdate').val());
        form_data.append('hdngravitypercUpdate', jQuery('#hdngravitypercUpdate').val());
        form_data.append('iscouponcodeUdpate', iscouponcodeUdpate);
        form_data.append('couponcodeUpate', jQuery('#couponcodeUpate').val());
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: settings_all_ajax_object.ajaxurl,
            data: form_data,
            success: function(response) {
                console.log(response);
                if (response.success == 1) {
                    Swal.fire(
                       'Data Updated successfully.',
                       '',
                       'success'
                   ).then(() => {
                       location.reload();
                   });
                }
            },
            processData: false,
            contentType: false,
        });
    });
    $('.modal-toggle-delete').on('click',function(e){
        e.preventDefault();
        var form_data = new FormData();
        form_data.append('action', 'playgretspinwheelsegmentDeleteform');
        form_data.append('lpNonce', jQuery('#lpNonce').val());
        form_data.append('delete_val', jQuery(this).attr('data-id'));
        jQuery.ajax({
            type: 'POST',
            dataType: 'json',
            url: settings_all_ajax_object.ajaxurl,
            data: form_data,
            success: function(response) {
                //console.log(response);
                if (response.success == 1) {
                    Swal.fire(
                       'Data Deleted successfully.',
                       '',
                       'success'
                   ).then(() => {
                       location.reload();
                   });
                }
            },
            processData: false,
            contentType: false,
        });
    });

    if (window.File && window.FileList && window.FileReader) {
        jQuery("#spinWheel_settings #files").on("change", function(e) {
          var files = e.target.files,
            filesLength = files.length;
          for (var i = 0; i < filesLength; i++) {
            var f = files[i]
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
              var file = e.target;
              console.log(file);
              //return false;
              $('#list').replaceWith("<ul class='gallery-thumbs filename bg-image'><li><img width=\"80\" height=\"80\" class=\"imageThumb\" src=\"" + file.result + "\"/>" +"<br/><span class=\"remove\"><i class=\"fa fa-trash fa-2x\"></i></span>" +"</li></ul>");
                jQuery(".remove").click(function(){
                    console.log('hello');
                    jQuery(".bg-image").remove();
                });
            });
            fileReader.readAsDataURL(f);
          }
          //console.log(files);
        });
      } else {
        alert("Your browser doesn't support to File API")
      }

      if (window.File && window.FileList && window.FileReader) {
        jQuery("#spinWheel_settings #markericon").on("change", function(e) {
          var files = e.target.files,
            filesLength = files.length;
          for (var i = 0; i < filesLength; i++) {
            var f = files[i]
            var fileReader = new FileReader();
            fileReader.onload = (function(e) {
              var file = e.target;
              //console.log(file);
              //return false;
              $('#marker_image').replaceWith("<ul class='gallery-thumbs filename bg-marker'><li><img width=\"80\" height=\"80\" class=\"imageThumb\" src=\"" + file.result + "\"/>" +"<br/><span class=\"remove\"><i class=\"fa fa-trash fa-2x\"></i></span>" +"</li></ul>");
                jQuery(".remove").click(function(){
                    console.log('hello');
                    jQuery(".bg-marker").remove();
                });
            });
            fileReader.readAsDataURL(f);
          }
          //console.log(files);
        });
      } else {
        alert("Your browser doesn't support to File API")
      }  
});