function shortcodeFunction(evt){    
    evt.preventDefault();
    var copyText = [teacher_information];
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    var elem = document.getElementById("myButton1");
    if (elem.value=="Copy Shortcode") elem.value = "Shortcode Copied";
    else elem.value = "Copy Shortcode";
}

jQuery(document).ready(function(){    
    jQuery( document ).on('click','#myButton1',function(e){
        e.preventDefault();
        var element = '[teacher_information]';
        CopyToClipboard( element );
        var text= (jQuery("#myButton1").val() === "Copy Shortcode" ? "Shortcode Copied" : "Copy Shortcode");
        jQuery("#myButton1").text(text);
    });
    
});

