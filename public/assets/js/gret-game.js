jQuery(document).ready(function($){
    // alert(wgp_facebook_object.fecebook_setting);
    var timeLeft = 30;
    $('#wheel-submit').on('click',function(){
        google_id  = $('#exampleInputgoogleid').val();
        facebook_id  = $('#exampleInputfacebookid').val();
        first_name = $('#exampleInputfirstname1').val();
        last_name = $('#exampleInputlastname1').val();
        email = $('#exampleInputEmail1').val();
        password = $('#exampleInputPassword1').val();
        confirm_password = $('#exampleInputConfirmPassword1').val();
        jQuery.ajax({
            url: wgp_ajax_object.ajaxurl,
            type: 'post',
            data: "action=get_gameListingfields&google_id=" + google_id + '&facebook_id=' + facebook_id + '&first_name=' + first_name+ '&last_name='+ last_name+'&email='+ email + '&password=' + password + '&confirm_password='+ confirm_password,
            success: function(response) {
                
                //console.log(response);
                //return false;
                var results = JSON.parse(response);
                //console.log(results.errors);
                if(results.success == 1){
                    $('#wgp-game-form').modal('hide');
                    Swal.fire(
                        'User Successfully Saved',
                        '',
                        'success'
                    ).then(function(){ 
                        location.reload();
                    });
                }else{
                    if(results.errors){
                        if(results.errors.first_name){
                            $('#exampleInputfirstname1').after("<div class='text-danger'>"+results.errors.first_name+"</div>");
                        }
                        if(results.errors.last_name){
                            $('#exampleInputlastname1').after("<div class='text-danger'>"+results.errors.last_name+"</div>");
                        }
                        if(results.errors.email){
                            $('#exampleInputEmail1').after("<div class='text-danger'>"+results.errors.email+"</div>");
                        }
                        if(results.errors.password){
                            $('#exampleInputPassword1').after("<div class='text-danger'>"+results.errors.password+"</div>");
                        }
                        if(results.errors.confirm_password){
                            $('#exampleInputConfirmPassword1').after("<div class='text-danger'>"+results.errors.confirm_password+"</div>");
                        }
                    }
                }
            }
        });
    });

    $('a.woocommerce-remove-coupon').on('click',function(e){
        setTimeout(function(){ 
            window.location='';
         }, 1200);
    });

    $('button[name="apply_coupon"]').on('click',function(e){
        e.preventDefault();
        var coupon_code_front =jQuery('#coupon_code').val();
        jQuery.ajax({
            url: wgp_ajax_object.ajaxurl,
            type: 'post',
            data:'action=gameListingcouponcodeelementcart&coupon_code_front=' + coupon_code_front,
            success: function(response) {     
                           
                var result = JSON.parse(response);                
                jQuery('.alert-dismissible, .text-danger').remove();
                jQuery('.woocommerce-notices-wrapper').removeClass('has-error');
                if (result.success == 0) {
                    if(result.message.coupon_code_not_exist){
                        jQuery('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error"><li>' + result.message.coupon_code_not_exist + '</li></ul>');
                    }
                    if(result.message.coupon_code_applied){
                        jQuery('.woocommerce-notices-wrapper').html('<ul class="woocommerce-info"><li>'+ result.message.coupon_code_applied +'</li></ul>');
                        window.location='';
                    }
                    if(result.message.coupon_code_already){
                        jQuery('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error"><li>'+ result.message.coupon_code_already +'</li></ul>');
                    }
                    if (result.message.coupon_code_front) {
                        jQuery('.woocommerce-notices-wrapper').html('<ul class="woocommerce-error"><li>' + result.message.coupon_code_front + '</li></ul>');
                    }
                    jQuery('.text-danger').parent().addClass('has-error');
                }else{
                    console.log(result.message);
                    if(result.message == 200){
                        jQuery('.woocommerce-notices-wrapper').html('<ul class="woocommerce-info"><li>Coupon code applied successfully.</li></ul>');
                        window.location='';
                    }
                }
            }
    
         });
    });

    $('#play-win').on('click',function(){
        var google_id = Cookies.get('google_id');
        var facebook_id = Cookies.get('facebook_id');
        console.log('play win click');

        if(Cookies.get('isloginstatus') == 1){
            console.log('wheel modal');
            $('#reach-you-wheel-Modal').modal('show');
            $('#form').modal('hide');
            $('#reach-you-form-Modal').modal('hide');       
        }else{
            $('#form').modal('show');
            // $('#reach-you-form-Modal').modal('show');
            var date = new Date();
            var minutes = 1;
            date.setTime(date.getTime() + minutes * 60 * 1000);
            $.cookie('the_cookie', 'true', {
                expires: date
            });
        }
    });
    
    $('#reach-you-form-Modal-start').click(function(){
        $('#reach-you-form-Modal').modal('show');
        $('#form').modal('hide');
    });
    $('#wgp-signup-form').click(function(){
        $('#wgp-game-form').modal('show');
        $('#form').modal('hide');
        
    });
    $(document).on('show.bs.modal', '#spin', function () {
        //alert('test');
        $('#reach-you-wheel-Modal').modal('hide');
    });
    $('#reward-coupon-Modal').on('click',function(){
        $('#spin').modal('hide');
        $('#coupon-Modal').modal('show');				
        var timerId = setInterval(countdown, 1000);
        console.log('wheel Auto open false');
        Cookies.set('wheel_auto_open', 'false');
        function countdown() {
            if (timeLeft < 0) {
                clearTimeout(timerId);
                $('#coupon-Modal').modal('hide');	                					
                // $('#earn-points-Modal').modal('hide');
                //window.location='';	
            } else {
                document.getElementById('Timer').innerHTML = timeLeft;
                timeLeft--;
            }
        }
    });
    $(document).on('show.bs.modal','#coupon-Modal',function(){

    });

    $(document).on('hide.bs.modal','#reach-you-form-Modal',function(){
        // console.log('hide modal and google login');
        window.location.reload();
    });
    $('#level-Modal').on('click',function(){				
        $('#earn-points-Modal').modal('hide');
        $('#level-Modal-cong').modal('show');
    });	
    $('#points-Modal').on('click',function(){				
        $('#level-Modal-cong').modal('hide');
        $('#points-Modal-earn').modal('show');
    });

    $('#level-Modal-Leaderboard').on('click',function(){
        $('#points-Modal-earn').modal('hide');
        $('#level-Modal-Leaderdashboard').modal('show');
    });
    
})


  
function copycouponFunction(copytext) {
    var copyText = document.getElementById(copytext);
    console.log(copyText);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand('copy');
    var coupon_code_element = document.getElementById(copytext).value;
    var coupon_id = document.getElementById(copytext).coupon_id;
    console.log(coupon_id);
    var user_name = '';
    if(Cookies.get('isloginstatus') == 1){
        if(Cookies.get('google_name')){
            user_name  = Cookies.get('google_name');
        }else if(Cookies.get('facebook_name')){
            user_name  = Cookies.get('facebook_name');
        }
    }else{
        var user_name = '';
    }
    var couponpoints = jQuery('#couponpoints').val();
    jQuery('.rewards').text(couponpoints);
    jQuery.ajax({
        url: wgp_ajax_object.ajaxurl,
        type: 'post',
        data:'action=gameListingcouponpointselement&couponpoints='+couponpoints+'&coupon_code_element='+coupon_code_element+'&user_name='+user_name,
        success: function(response) {
            var result = JSON.parse(response);
            if (result.success==0) {
                Swal.fire(

                    result.message,

                    '',

                    'error'

                ).then(function(){ 
                    location.reload();
                    }
                );
                jQuery('#coupon-Modal').modal('hide');
            }else{
                jQuery('#coupon-Modal').modal('hide');
                jQuery('#earn-points-Modal').modal('show');    
            }
            //console.log(response);
            //return false;
        }

    });
    jQuery.ajax({
        url: wgp_ajax_object.ajaxurl,
        type: 'post',
        data:'action=gameListingcouponcodeelement&coupon_code_element='+coupon_code_element,
        success: function(response) {
            //console.log(response);
            //return false;
            
        }

     });
  }
  


// Gmail Login With js
// Render Google Sign in button
// Render Google Sign in button
function renderButton() {
    gapi.signin2.render('gSignIn', {
        'scope': 'profile email',
        'width': 240,
        'height': 50,
        'longtitle': true,
        'theme': 'dark',
        'onsuccess': onSuccess,
        'onfailure': onFailure
    });
}

// Sign-in success callback
function onSuccess(googleUser) {
    // Get the Google profile data (basic)
    //var profile = googleUser.getBasicProfile();
    
    // Retrieve the Google account data
    gapi.client.load('oauth2', 'v2', function () {
        var request = gapi.client.oauth2.userinfo.get({
            'userId': 'me'
        });
        request.execute(function (resp) {
            // Display the user details
            console.log(resp);
            if(resp.email != ''){
                console.log('google details enter');
                profileHTML = '<div class="lp-join-now"><span class="user-details">Hi '+resp.given_name+'! <a href="'+wgp_facebook_object.logout_url+'" onclick="signOut();">Sign out</a></span></div>';
                // profileHTML += '<img src="'+resp.picture+'"/><p><b>Google ID: </b>'+resp.id+'</p><p><b>Name: </b>'+resp.name+'</p><p><b>Email: </b>'+resp.email+'</p><p><b>Gender: </b>'+resp.gender+'</p><p><b>Locale: </b>'+resp.locale+'</p><p><b>Google Profile:</b> <a target="_blank" href="'+resp.link+'">click to view profile</a></p>';
                // document.getElementsByClassName("userContent")[0].innerHTML = profileHTML;
                console.log(profileHTML);
                document.getElementById("gSignIn").style.display = "none";
                document.getElementsByClassName("userContent")[0].style.display = "block";

                $('#userContent').html(profileHTML);
                $('#userContent').attr('style', 'display:block;');

                Cookies.set('google_name', resp.name);
                Cookies.set('google_email', resp.email);
                Cookies.set('google_id', resp.id);
                Cookies.set('isloginstatus', 1);

                // $('#reach-you-wheel-Modal').modal('show');
                $('#form').modal('hide');
                $('#reach-you-form-Modal').modal('hide');       

                saveUserData(resp);
            }else{
                Cookies.set('google_name', 'undefined');
                Cookies.set('google_email', 'undefined');
                Cookies.set('google_id', 'undefined');
                Cookies.set('isloginstatus', 0);

                // $('#form').modal('show');
            }
            
        });
    });
}

// Sign-in failure callback
function onFailure(error) {
    console.log(error);
}

// Sign out the user
function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
        document.getElementsByClassName("userContent")[0].innerHTML = '';
        document.getElementsByClassName("userContent")[0].style.display = "none";
        document.getElementById("gSignIn").style.display = "block";
    });
    
    auth2.disconnect();

    Cookies.set('google_name', 'undefined');
    Cookies.set('google_email', 'undefined');
    Cookies.set('google_id', 'undefined');
    Cookies.set('wheel_auto_open', 'undefined');
    Cookies.set('isloginstatus', 0);
}

// console.log(fb_app_id);
// Facebook Code With js
fb_app_id = wgp_facebook_object.fecebook_setting;
window.fbAsyncInit = function() {
    // FB JavaScript SDK configuration and setup
    FB.init({
    //   appId      : '333120031915502', // FB App ID
      appId      : fb_app_id, // FB App ID
      cookie     : true,  // enable cookies to allow the server to access the session
      xfbml      : true,  // parse social plugins on this page
      version    : 'v3.2' // use graph api version 2.8
    });
    
    // Check whether the user already logged in
    FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
            //display user data
            getFbUserData();
        }
    });
};

// Load the JavaScript SDK asynchronously
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Facebook login with JavaScript SDK
function fbLogin() {
    FB.login(function (response) {
        if (response.authResponse) {
            // Get and display the user profile data
            getFbUserData();
        } else {
            // document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
        }
    }, {scope: 'email'});
}

// Fetch the user profile data from facebook
function getFbUserData(){
    FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture'},
    function (response) {
        // document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
        // document.getElementById('fbLink').innerHTML = 'Logout from Facebook';
        // document.getElementById('status').innerHTML = '<p>Thanks for logging in, ' + response.first_name + '!</p>';
        // document.getElementById('userData').innerHTML = '<h2>Facebook Profile Details</h2><p><img src="'+response.picture.data.url+'"/></p><p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>FB Profile:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';

        profileHTML = '<span class="user-details">Hi '+response.first_name+' <a href="javascript:void(0);" class="app-view-popup-style" onclick="fbLogout();">Log Out</a></span>';
        // document.getElementsByClassName("userContent")[0].innerHTML = profileHTML;

        document.getElementById("fbLink").style.display = "none";
        // document.getElementsByClassName("userContent")[0].style.display = "block";
        document.getElementsByClassName("app-view-popup-style")[0].style.display = "block";

        document.getElementById("facebook_name").value = response.name;
        document.getElementById("facebook_email").value = response.email;
        document.getElementById("facebook_id").value = response.id;

        if(response.id != ''){
            $('#reach-you-wheel-Modal').modal('show');
            $('#form').modal('hide');
            $('#reach-you-form-Modal').modal('hide');
        }else{
            $('#form').modal('show');
        }
        Cookies.set('facebook_name', response.first_name+' '+response.last_name);
        Cookies.set('facebook_email', response.email);
        Cookies.set('facebook_id', response.id);
        Cookies.set('isloginstatus', 1);
        console.log('fb user detail enter');
        saveFBUserData(response);
    });
}

// Logout from facebook
function fbLogout() {
    FB.logout(function() {
        // document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
        // document.getElementById('fbLink').innerHTML = '<img src="images/fb-login-btn.png"/>';
        document.getElementById('userData').innerHTML = '';
        // document.getElementById('status').innerHTML = '<p>You have successfully logout from Facebook.</p>';

        Cookies.set('facebook_name', 'undefined');
        Cookies.set('facebook_email', 'undefined');
        Cookies.set('facebook_id', 'undefined');
        Cookies.set('wheel_auto_open', 'undefined');
        Cookies.set('isloginstatus', 0);

        // disconnect after reload
        window.location.reload();	
        location.reload();
    });
}


// Logout from facebook
function fb_Logout() {
    document.getElementById('userData').innerHTML = '';
    Cookies.set('facebook_name', 'undefined');
    Cookies.set('facebook_email', 'undefined');
    Cookies.set('facebook_id', 'undefined');
    Cookies.set('wheel_auto_open', 'undefined');
    Cookies.set('isloginstatus', 0);
    // disconnect after reload
    window.location.reload();	
    location.reload();
}
    
// Insert User data
function saveUserData(res){
    var name = res.name;
    var email = res.email;
    var google_id = res.id;
    // console.log('G login Enter');
    jQuery.ajax({
        url: wgp_ajax_object.ajaxurl,
        type: 'post',
        data: "action=get_gameListingfields&google_id="+google_id+'&name='+name+'&email='+ email+'&type=google',
        success: function(response) {
            console.log(response);
        }
    });
}

function saveFBUserData(response){
    var name = response.first_name+' '+response.last_name;
    var first_name = response.first_name;
    var email = response.email;
    var facebook_id = response.id;
    // console.log('G login Enter');
    jQuery.ajax({
        url: wgp_ajax_object.ajaxurl,
        type: 'post',
        data: "action=get_gameListingfields&facebook_id="+facebook_id+'&name='+name+'&email='+ email+'&first_name='+first_name+'&type=facebook',
        success: function(response) {
            console.log(response);
        }
    });
}

