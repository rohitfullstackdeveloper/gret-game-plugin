<?php


/**
 * The frontend-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the frontend-specific stylesheet and JavaScript.
 *
 * @package    WPLI
 * @subpackage WPLI/admin
 * @author     Rohit Sharma
 */
class TTE_Frontend {
    private $plugin_name,$version;

    public function __construct( $WPLI, $version ) {
        $this->plugin_name = $WPLI;
        $this->version = $version;
        add_action( 'wp_enqueue_scripts', array($this,'wgp_frontend_styles' ));
        add_shortcode( 'game_popup_settings', array($this,'wgp_game_popup_registration' ));
		add_action('init', array($this,'wgp_game_session' ));

        //add_action( 'init', array($this,'wgp_add_role' ));
		
    }

   public function wgp_game_session(){
		if (!session_id())
		session_start();
	}

    public function wgp_frontend_styles(){
    	wp_deregister_script('jquery');
    	wp_enqueue_script('jquery','https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js');
		wp_enqueue_style('wgp-frontend-boostrap4.3', APP_URL.'public/assets/css/bootstrap.min.css', array(), false, false);
		wp_enqueue_style('wgp-frontend-font-awesome4.0', APP_URL.'public/assets/css/bootstrap.min.css', array(), false, false);
    	wp_enqueue_style('wgp-frontend', APP_URL.'public/assets/css/frontend_css.css', array(), false, false);    	
		wp_enqueue_script( 'wgp-boostrap-bundle4.6-js',APP_URL.'public/assets/js/bootstrap.bundle.min.js');	
		wp_enqueue_script( 'wgp-boostrap-popper5.0-js',APP_URL.'public/assets/js/popper.min.js');
		wp_enqueue_script( 'wgp-boostrap-5.0-js',APP_URL.'public/assets/js/bootstrap.min.js');	
    	wp_enqueue_script( 'wgp-sweetalert-js','https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.js');
    	wp_enqueue_style( 'wgp-sweetalert-style', 'https://cdn.jsdelivr.net/npm/sweetalert2@10/dist/sweetalert2.min.css');
    	wp_enqueue_script( 'wgp-custom-js',APP_URL.'public/assets/js/tipsy-js.js');
    	wp_localize_script( 'wgp-custom-js', 'wgp_ajax_object',  array('ajaxurl' => admin_url( 'admin-ajax.php' )));
		
    }


    // public function wgp_add_role(){
    // 	add_role( __('teacher', 'tte'), __('Teacher', 'tte'), get_role( 'subscriber' )->capabilities );
    // }

    public function wgp_game_popup_registration(){
    	ob_start();
		
    ?>
	<div class="sidebar-background-second ">
			<span data-toggle="modal" data-target="#form">GAME</span> 
	</div>
	<div class="modal fade" id="form" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-dialog-centered" role="document">
	        <div class="modal-content px-md-4 p-sm-3 p-4">
	            <div class="image"> 
	            	<img src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/controller-image.jpg'?>" class="text-center">
            	</div>
            	<p class="r3 px-sm-1">Play games with us and win rewards!</p>

            	<div class="text-center mb-3 text-button start">
            		<a data-toggle="modal" data-target="#reach-you-form-Modal">START
        				<img src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/icon.jpg'?>">
        			</a>
            	</div>
	        	
	    	</div>
		</div>
	</div>
	<!-- reach-you-form-Modal -->
	<?php if ( is_user_logged_in() ) { ?>
		<div class="modal fade" id="reach-you-form-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabelNextTime" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content text-center">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabelNextTime"></h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="image p-4"> 
						<img src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/wheel.png'?>" class="text-center">
					</div>
				</div>
			</div>
		</div>	
	<?php } else { 
		// unset($_SESSION);
		//session_destroy();
		//echo "<pre>";print_r($_SESSION);exit;
		$game_option_auth = get_option('game_options');
		$setClientId = $game_option_auth['game_google_api_keys'];
		$setClientSecret = $game_option_auth['game_google_secret_keys'];
		$redirectUri = 'http://localhost/wp-olaplex/';
		$client = new Google_Client();
		$client->setClientId($setClientId);
		$client->setClientSecret($setClientSecret);
		$client->setRedirectUri($redirectUri);
		$client->addScope("email");
		$client->addScope("profile");
		// authenticate code from Google OAuth Flow		
		//echo "<pre>";print_r($_GET);exit;
		//echo "<pre>";print_r($_SESSION);exit;
		if (isset($_GET['code'])) {
			// wp_redirect( 'https://artisansweb.net/redirect-old-url-new-url-without-plugin-wordpress/', 301 );
			
			// $client->authenticate($_GET['code']);
			// $_SESSION['access_token'] = $client->getAccessToken();
			// $google_oauth = new Google_Service_Oauth2($client);
			// $google_account_info = $google_oauth->userinfo->get();
			// $_SESSION['google_email'] =  $google_account_info->email;
			// $_SESSION['google_name'] =  $google_account_info->name;	
			// if($_SESSION['access_token']){
			// 	echo "<a href='http://localhost/wp-olaplex'>Home Page</a>";
			// 	exit;
			// }
			$token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
			$client->setAccessToken($token);
			$gauth = new Google_Service_Oauth2($client);
			$_SESSION['g_token']=$client->getAccessToken();
			$google_url = '';
			if(isset($_SESSION['g_token'])){
				$google_access_token =$_SESSION['g_token']['access_token'];
				$google_info = $gauth->userinfo->get();
				$_SESSION['email'] = $google_info->email;
				$_SESSION['firstName'] = $google_info->givenName;
				$_SESSION['lastName'] = $google_info->familyName;
				$_SESSION['googleId'] = $google_info->id;
				//echo "Welcome ".$firstName." with surname of =".$lastName." and You are registered using email:".$email." with googleId:".$googleId." with:".$google_access_token;
			}
			
		// now you can use this profile info to create account in your website and make user logged in.
		} else {
			$google_url =  "<a href='".$client->createAuthUrl()."'>Google Login</a>";
		}
		
		
		// Facebook Login
		$setFbClientId = $game_option_auth['game_facebook_api_keys'];
		$setFbClientSecret = $game_option_auth['game_facebook_secret_keys'];
		$fb = new Facebook\Facebook([
			'app_id' => $setFbClientId,
			'app_secret' => $setFbClientSecret,
			'default_graph_version' => 'v2.10',
		]);
		$helper = $fb->getRedirectLoginHelper();
		$permissions = ['email']; // optional
		if (isset($_SESSION['facebook_access_token'])) {
			$fbaccessToken = $_SESSION['facebook_access_token'];
		} else {
			$fbaccessToken = $helper->getAccessToken();
		}

		$fb_login = '';
		
		if (isset($fbaccessToken)) {
			if (isset($_SESSION['facebook_access_token'])) {
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			} else {
				// getting short-lived access token
				$_SESSION['facebook_access_token'] = (string) $fbaccessToken;
				// OAuth 2.0 client handler
				$oAuth2Client = $fb->getOAuth2Client();
				// Exchanges a short-lived access token for a long-lived one
				$longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($_SESSION['facebook_access_token']);
				$_SESSION['facebook_access_token'] = (string) $longLivedAccessToken;
				$fb->setDefaultAccessToken($_SESSION['facebook_access_token']);
			}
		
			// redirect the user to the profile page if it has "code" GET variable
			if (isset($_GET['code'])) {	
				if($_SESSION['facebook_access_token']){
					echo "<a href='http://localhost/wp-olaplex'>Home Page</a>";
					exit;
				}	
			}
			$profile_request = $fb->get('/me?fields=name,first_name,last_name,email');
			$requestPicture = $fb->get('/me/picture?redirect=false&height=200'); //getting user picture
			$picture = $requestPicture->getGraphUser();
			$profile = $profile_request->getGraphUser();
			$fbid = $profile->getProperty('id');           // To Get Facebook ID
			$fbfullname = $profile->getProperty('name');   // To Get Facebook full name
			$fbemail = $profile->getProperty('email');    //  To Get Facebook email
			$fbpic = "<img src='".$picture['url']."' class='img-rounded'/>";
			# save the user nformation in session variable
			$_SESSION['fb_id'] = $fbid.'</br>';
			$_SESSION['fb_name'] = $fbfullname.'</br>';
			$_SESSION['fb_email'] = $fbemail.'</br>';
			$_SESSION['fb_pic'] = $fbpic.'</br>';
		} else {
			// replace your website URL same as added in the developers.Facebook.com/apps e.g. if you used http instead of https and you used            
			$loginUrl = $helper->getLoginUrl('http://localhost/wp-olaplex/', $permissions);
			$fb_login = '<a href="' . $loginUrl . '">FB!</a>';
		}
		
		?>
		<div class="modal fade" id="reach-you-form-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLongTitle"></h5>
						<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
					</div>
					<div class="modal-body reach-text" id="text-upper">
						<div class="upper-text">
							<a data-toggle="modal" data-target="#register-form-Modal" id="to-recover" class="upper-text">How can we reach you?</a>
						</div>
						<div class="bottom-text">
							<a data-toggle="modal" data-target="#wgp-game-form" id="register-form" class="register-text">Show registration option via 
								<?php if(empty($_SESSION['g_token']['access_token'])){
									echo $google_url;	
								}else{ ?>
									<a data-toggle="modal" data-target="#wgp-game-form" id="wgp-game-form-modal" class="upper-text">Google</a>
								<?php } ?>
								<?php if($fb_login){
									echo $fb_login; 
								}else{
								?>
									<a data-toggle="modal" data-target="#wgp-game-form" id="to-recover" class="upper-text">Fb</a>
								<?php } ?>
							</a>
						</div>
					</div>
					<div class="modal-footer"></div>
				</div>
			</div>
		</div>	
		
	<?php }?>	

	<div class="bg-primary-dark position-relative overflow-hidden">
		<div class="container content-space-2">
			<div class="row justify-content-center align-items-lg-center">
				<div class="col-md-8 background-first col-lg-6 mb-7 mb-lg-0">
					<div class="pe-lg-3 mb-7">
						<div class="display-3 text-black mb-3 mb-md-5">
							New OLAPLEX Swag
						</div>
						<p class="lead text-black-70">
							Get A free Scrunchie Set with select duos available ONLY ON Olaplex.com.<br>$56, A $62 Value
						</p>
						<p class="lead text-black-701">
							SHOP THE DAILY CLEANSE & CONDITION DUO
						</p>
					</div>
				</div>
				<div class="col-md-8 background-second col-lg-6 mb-7 mb-lg-0">
					<div class="pe-lg-3 mb-7">
						<div class="display-3-3 text-black mb-3 mb-md-5">						
							<img class="navbar-brand-logo" src="<?php echo plugin_dir_url( __FILE__ ). 'assets/images/product-image.jpg'?>">
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="wgp-game-form" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalCenterTitle"></h5>
	        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
	      </div>
	      <div class="modal-body reach-text" id="text-upper">
	        <div class="upper-text">
	        	<h3 id="to-recover" class="upper-text">Register Here</h3>
	        </div>
	        <div class="form-register">
	        	<form>
	        		<div class="form-group">						
						<input type="text" class="form-control" id="exampleInputfirstname1" value ="<?php echo $_SESSION['firstName'];?>" placeholder="First-Name">
						<input type="hidden" class="form-control" id="exampleInputgoogleid" value ="<?php echo $_SESSION['googleId'];?>">
				  	</div>
					<div class="form-group">						
						<input type="text" class="form-control" id="exampleInputlastname1" value ="<?php echo $_SESSION['lastName']?>" placeholder="Last-Name">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="exampleInputEmail1" value ="<?php echo $_SESSION['email'];?>" placeholder="Email">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
					</div>
					<div class="form-group">
						<input type="password" class="form-control" id="exampleInputConfirmPassword1" placeholder="Confirm-Password">
					</div>
					<div class="button-form">
						<button data-toggle="modal" data-target="#wheel-Modal" class="btn btn-primary btn-submit">Submit</button>
					</div>
				</form>
	        </div>
	      </div>
	      <div class="modal-footer">
	      </div>
	    </div>
	  </div>
	</div>
	<?php 
	$popup_second = get_option('game_options');
	?>
	<script>
		var second = '<?php echo $popup_second['game_start_popup_seconds'];?>';
		var finaltime=  parseInt(second)*1000;
		(function($){
			setInterval(function () {
				$('#form').modal('show');
			}, finaltime);
			
			$('.start').click(function(){
				$('#reach-you-form-Modal').modal('show');
				$('#form').modal('hide');
			});

			$('#wgp-game-form-modal').click(function(){
				$('#wgp-game-form').modal('show');
				$('#reach-you-form-Modal').modal('hide');
				$('#form').modal('hide');

			});

		})(jQuery)
	</script>
	<?php
	return ob_get_clean();
    }
	
}



new TTE_Frontend( 'WP Game Plugin','1.0.0' );