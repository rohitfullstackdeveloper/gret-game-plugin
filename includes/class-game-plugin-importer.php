<?php
/**
 * @since      1.0.0
 * @package    WGP
 * @subpackage WGP/includes
 * @author     Rohit Sharma
 */

class game_plugin_importer {
	protected $loader,$plugin_name,$version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */

	public function __construct() {
		$this->plugin_name 	= 'WGP';
		$this->version 		= '1.0.0';
		$this->load_dependencies();
	}

	/**
	 * @description  Load the required dependencies for this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @author   Rohit Sharma
	 */

	private function load_dependencies() {
		// Load Helper File
		require_once plugin_dir_path( __DIR__ ).'includes/class-game-plugin-helper.php';

		// Load Admin File
		require_once plugin_dir_path( __DIR__ ).'admin/class-game-plugin-admin.php';

		// Load Frontend File
		require_once plugin_dir_path( __DIR__ ).'public/class-game-plugin-frontend.php';
	}

	/**
	 * @description The name of the plugin used to uniquely identify it within the context of
	 *	  			WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 * @author    Rohit Sharma
	 */

	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * @description Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 * @author    Rohit Sharma
	 */

	public function get_version() {
		return $this->version;
	}
}