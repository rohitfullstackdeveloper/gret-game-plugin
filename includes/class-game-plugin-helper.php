<?php
/**
 * @since      1.0.0
 * @package    WPLI
 * @subpackage WPLI/includes
 * @author     Rohit Sharma
 */

class game_plugin_helper {
	protected $loader,$plugin_name,$version;

	public function __construct() {
		$this->plugin_name 	= 'WPLI';
		$this->version 		= '1.0.0';
	}

	public function clean($string) {
		$string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
		return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
	}

	public function user_login($user_api_detail){
		$urlApi = WGP_BASE_URL.'profile/get_user_token/';
		//$email  = $_SESSION['email'];
		$request_array = array(
			'email' 				=> $_REQUEST['email']
		);		
		//echo "<pre>";print_r($request_array);exit;
		$api_response = wp_remote_post( $urlApi, array(
			'method'      			=> 'POST',
			'headers'       		=>array('Content-Type' => 'application/json'),
			'body'       			=> json_encode($request_array)
			)
		);		
		$json_response = json_decode($api_response['body']);
		$token = $json_response->token;			
		$getuserApiToken = get_option('user_api_detail');
		//echo "<pre>";print_r($getuserApiToken);exit;
		if(false !== $getuserApiToken){
			if($getuserApiToken === $token){

			}else{
				//echo $getuserApiToken ;exit;
				$user_api_detail = update_option('user_api_detail',$token,'yes');
			}
		}else{
			$user_api_detail = add_option('user_api_detail',$token);
		}
		return $user_api_detail;
	}

	public function getUserLevel(){
		$getuserApi = get_option('user_api_detail');
		$url = WGP_BASE_URL.'xp/get_user_level';
		$request_array = array(
			'Authorization' 				=> 'token'.' '.$getuserApi
		);		
		$api_response = wp_remote_get( $url, array(
			'method'      	  => 'GET',
			'headers'	  	  => $request_array
			)
		);
		$jsonResponseLevel = json_decode($api_response['body']);
		// echo "<pre>";print_r($jsonResponseLevel);exit;
		$level = $jsonResponseLevel->level;
		return $level;
	}

	public function getUserprofile(){
		$getuserApi = get_option('user_api_detail');
		$url = WGP_BASE_URL.'profile/get_profile_details';
		$request_array = array(
			'Authorization'					=> 'token'.' '.$getuserApi
		);
		
		$api_response = wp_remote_get( $url, array(
			'method'      => 'GET',
			'headers'        => $request_array,
			)
		);
		$jsonResponseProfile = json_decode($api_response['body']); 		
		// echo "<pre>";print_r($jsonResponseProfile);exit;
		if($jsonResponseProfile){
			return $jsonResponseProfile;
		}else{
			return false;
		}
	}

	public function getLeaderBoardData(){
		$getuserApi = get_option('user_api_detail');
		$url = WGP_BASE_URL.'profile/get_leader_board_data';
		$request_array = array(
			'Authorization'					=> 'token'.' '.$getuserApi
		);
		$api_response = wp_remote_get( $url, array(
			'method'      => 'GET',
			'headers'        => $request_array,
			)
		);
		$jsonResponseLeaderBoardData = json_decode($api_response['body']); 	
		// echo "<pre>";print_r($jsonResponseLeaderBoardData);exit;
		if($jsonResponseLeaderBoardData){
			return $jsonResponseLeaderBoardData;
		}else{
			return false;
		}
	}
	
	public function redeemCoupon($order_id){
		$getuserApi = get_option('user_api_detail');
		$order = wc_get_order( $order_id );
		$order_value = (int)$order->get_total();
		// echo '<pre>';print_r($order_value);exit;
		$coupon_id = ''; 
		if( !empty($order->get_used_coupons()) ){
			$coupon_ids = array();
			foreach( $order->get_used_coupons() as $coupon_code ){
				// Retrieving the coupon ID
				$coupon_post_obj = get_page_by_title($coupon_code, OBJECT, 'shop_coupon');
				$coupon_ids[]       = $coupon_post_obj->post_title;
				// $coupon_ids[] = $coupon_code;
			}
			$coupon_id = implode( ',',  $coupon_ids );
		}else{
			$coupon_id = '';
		}
		// echo '<pre>';print_r($coupon_id);exit;

		// Customer billing information details
		$billing_first_name = $order->get_billing_first_name();
		$billing_last_name  = $order->get_billing_last_name();
		// $customer_name  = $billing_first_name .' '. $billing_last_name;
		if($_SESSION['google_name']){
			$customer_name = $_SESSION['google_name'];
		}elseif($_SESSION['facebook_name']){
			$customer_name = $_SESSION['facebook_name'];
		}else{
			$customer_name  = '';
		}
		
		$coupon_array =  array(
			'order_id' => $order_id,
			'coupon_id' => $coupon_id,
			'order_value' => $order_value,
			'client' => $customer_name,
		);
		$coupon_data = json_encode($coupon_array);
		
		$url = WGP_BASE_URL.'coupons/redeem_coupon/';

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => $url,
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS =>$coupon_data,
		CURLOPT_HTTPHEADER => array(
			'Authorization: token'.' '.$getuserApi,
			'Content-Type: application/json',
		),
		));

		$response = curl_exec($curl);
		
		curl_close($curl);
		$jsonResponseclaimCouponData = json_decode($response); 	
		if($jsonResponseclaimCouponData->success == 1){
			return $jsonResponseclaimCouponData->points;
		}else{
			return 0;
		}
	}

}