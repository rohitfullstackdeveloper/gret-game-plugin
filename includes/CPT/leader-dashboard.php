<?php

/**
 * The template for displaying Leader Dashboard Page.
 * @version 2.0
 */
?>
<head>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href=<?php echo WGP_APP_URL.'public/assets/css/frontend_css.css';?>>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.0/css/font-awesome.css" crossorigin="anonymous">
</head>
<div class="container dashboard-bg">
      <div class="col-12 leader-title pr-0 pl-0">
      		<span>LEADERBOARD</span>
      </div>
      <div class="row profile-main">
		  <div class="col-sm-4 main-box" style="padding-top:50px;padding-right: 30px;padding-left: 30px;">
		    <div class="card profile-box">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-5 pr-0">
			      		<img class="left-medal" img src="<?php echo WGP_APP_URL. 'public/assets/images/medal-2.jpg'?>">
			      		<div class="text-pts">
			      			500 pts
			      		</div>
			      	</div>
			      	<div class="col-sm-7 profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/profile-1-img.png'?>">
			    	</div>
		    	</div>
		    	<div class="w-100 description">
		        	<h5 class="card-title pt-3">Vikram Singh</h5>
		    	</div>
		    	<div class="w-100 description2">
		        	<h5 class="card-title pt-2">New Face</h5>
		    	</div>
		    	<div class="w-100 d-flex achieve-front-text">
			      	<div class="col-sm-6 pr-0">
			      		<span class="text-achieve">
			      			Achievements:
			      		</span>
			      	</div>
			      	<div class="col-sm-6 medal-person pl-0 pr-0">
			        	<img class="img-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/award-2.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		  <div class="col-sm-4 main-box"style="padding-right: 30px;padding-left: 30px;">
		    <div class="card profile-box">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-5 pr-0">
			      		<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/first-medal.jpg'?>">
			      		<div class="text-pts">
			      			565 pts
			      		</div>
			      	</div>
			      	<div class="col-sm-7 profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/agarwal-img.png'?>">
			    	</div>
		    	</div>
		    	<div class="w-100 description">
		        	<h5 class="card-title pt-3">Dinesh Agarwal</h5>
		    	</div>
		    	<div class="w-100 description2">
		        	<h5 class="card-title pt-2">New Face</h5>
		    	</div>
		    	<div class="w-100 d-flex achieve-front-text">
			      	<div class="col-sm-5 pr-0">
			      		<span class="text-achieve">
			      			Achievements:
			      		</span>
			      	</div>
			      	<div class="col-sm-7 medal-person pl-0 pr-0">
			        	<img class="img-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/dinesh-award.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		   <div class="col-sm-4 main-box" style="padding-top:50px;padding-right: 30px;padding-left: 30px;">
		    <div class="card profile-box">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-5 pr-0">
			      		<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/third-medal.jpg'?>">
			      		<div class="text-pts">
			      			540 pts
			      		</div>
			      	</div>
			      	<div class="col-sm-7 profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/girl-img.png'?>">
			    	</div>
		    	</div>
		    	<div class="w-100 description">
		        	<h5 class="card-title pt-3">Anwesha Dey</h5>
		    	</div>
		    	<div class="w-100 description2">
		        	<h5 class="card-title pt-2">New Face</h5>
		    	</div>
		    	<div class="w-100 d-flex achieve-front-text">
			      	<div class="col-sm-6 pr-0">
			      		<span class="text-achieve">
			      			Achievements:
			      		</span>
			      	</div>
			      	<div class="col-sm-6 medal-person pl-0 pr-0">
			        	<img class="img-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/award-3.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="row profile-lower">
			<div class="col-sm-12 lower-box" style="padding-top:50px;padding-right: 40px;padding-left: 40px;">
		    <div class="card profile-box2 p-4">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-1 pr-0 circle-height">
			      		<div class="circle-num">
			      			<span>4</span>
			      		</div>
			      	</div>
			      	<div class="col-sm-3 botoom-profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/round-pic.png'?>">
			        	<span>Jackob Neilson</span>
			    	</div>
			    	<div class="col-sm-2 botoom-pts pr-0">
			        	<span>395 pts</span>
			    	</div>
			    	<div class="col-sm-2 botoom-face pr-0">
			        	<span>New-Face</span>
			    	</div>
			    	<div class="col-sm-4 botoom-award pr-0">
			        	<img class="bottom-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/bottom-medal.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="row profile-lower">
			<div class="col-sm-12 lower-box" style="padding-top:50px;padding-right: 40px;padding-left: 40px;">
		    <div class="card profile-box2 p-4">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-1 pr-0 circle-height">
			      		<div class="circle-num">
			      			<span>5</span>
			      		</div>
			      	</div>
			      	<div class="col-sm-3 botoom-profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/round-pic.png'?>">
			        	<span>Jackob Neilson</span>
			    	</div>
			    	<div class="col-sm-2 botoom-pts pr-0">
			        	<span>395 pts</span>
			    	</div>
			    	<div class="col-sm-2 botoom-face pr-0">
			        	<span>New-Face</span>
			    	</div>
			    	<div class="col-sm-4 botoom-award pr-0">
			        	<img class="bottom-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/bottom-medal.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="row profile-lower">
			<div class="col-sm-12 lower-box" style="padding-top:50px;padding-right: 40px;padding-left: 40px;">
		    <div class="card profile-box2 p-4">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-1 pr-0 circle-height">
			      		<div class="circle-num">
			      			<span>6</span>
			      		</div>
			      	</div>
			      	<div class="col-sm-3 botoom-profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/round-pic.png'?>">
			        	<span>Jackob Neilson</span>
			    	</div>
			    	<div class="col-sm-2 botoom-pts pr-0">
			        	<span>395 pts</span>
			    	</div>
			    	<div class="col-sm-2 botoom-face pr-0">
			        	<span>New-Face</span>
			    	</div>
			    	<div class="col-sm-4 botoom-award pr-0">
			        	<img class="bottom-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/bottom-medal.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="row profile-lower">
			<div class="col-sm-12 lower-box" style="padding-top:50px;padding-right: 40px;padding-left: 40px;">
		    <div class="card profile-box2 p-4">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-1 pr-0 circle-height">
			      		<div class="circle-num">
			      			<span>7</span>
			      		</div>
			      	</div>
			      	<div class="col-sm-3 botoom-profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/round-pic.png'?>">
			        	<span>Jackob Neilson</span>
			    	</div>
			    	<div class="col-sm-2 botoom-pts pr-0">
			        	<span>395 pts</span>
			    	</div>
			    	<div class="col-sm-2 botoom-face pr-0">
			        	<span>New-Face</span>
			    	</div>
			    	<div class="col-sm-4 botoom-award pr-0">
			        	<img class="bottom-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/bottom-medal.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
		<div class="row profile-lower">
			<div class="col-sm-12 lower-box" style="padding-top:50px;padding-right: 40px;padding-left: 40px;padding-bottom: 50px;">
		    <div class="card profile-box2 p-4">
		      <div class="card-body p-0">
		      	<div class="w-100 d-flex medal-img">
			      	<div class="col-sm-1 pr-0 circle-height">
			      		<div class="circle-num">
			      			<span>8</span>
			      		</div>
			      	</div>
			      	<div class="col-sm-3 botoom-profile-person pr-0">
			        	<img class="left-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/round-pic.png'?>">
			        	<span>Jackob Neilson</span>
			    	</div>
			    	<div class="col-sm-2 botoom-pts pr-0">
			        	<span>395 pts</span>
			    	</div>
			    	<div class="col-sm-2 botoom-face pr-0">
			        	<span>New-Face</span>
			    	</div>
			    	<div class="col-sm-4 botoom-award pr-0">
			        	<img class="bottom-medal"img src="<?php echo WGP_APP_URL. 'public/assets/images/bottom-medal.jpg'?>">
			    	</div>
		    	</div>
		      </div>
		    </div>
		  </div>
		</div>
</div>
<?php
 ?>
